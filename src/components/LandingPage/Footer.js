// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
import ReactSVG from "react-svg";
// IMPORT components
import IconButtonOutsideLink from "components/UI/Button/IconButtonOutsideLink";
import SocialNetworksWrapper from "components/UI/Wrapper/SocialNetworks";
import { PublicLogoWrapper } from "components/UI/Wrapper/Logo";
import { WhiteTypography } from "components/UI/Typography";
import LogoImg from "components/UI/Img/Logo";
// IMPORT images
import Logo from "imgs/logo.svg";
// Custom Styles
const FooterWrapper = styled.div`
   {
    background: ${props => props.theme.background.purple};
    padding: ${props => props.theme.spacing.xl}px;
    color: ${props => props.theme.text.footer};
    text-align: center;
    display: flex;
    align-items: center;
    flex-direction: column;
  }
`;

const SubTitleWrapper = styled.div`
   {
    margin: ${props => props.theme.spacing.xxxl}px 0;
  }
`;
// Component: Stateless
const LandingPageFooter = ({ subtitle, socialNetworks }) => (
  <FooterWrapper>
    <PublicLogoWrapper>
      <LogoImg color="footer" size="public">
        <ReactSVG path={Logo} />
      </LogoImg>
    </PublicLogoWrapper>
    <SubTitleWrapper>
      <WhiteTypography variant="title">
        <FormattedMessage id={subtitle} />
      </WhiteTypography>
    </SubTitleWrapper>
    <SocialNetworksWrapper>
      {socialNetworks.map(site => (
        <IconButtonOutsideLink
          key={site.link}
          link={site.link}
          icon={site.icon}
          iconSize="large"
        />
      ))}
    </SocialNetworksWrapper>
  </FooterWrapper>
);
// PropTypes
LandingPageFooter.propTypes = {
  subtitle: PropTypes.string.isRequired,
  socialNetworks: PropTypes.array.isRequired
};

export default LandingPageFooter;
