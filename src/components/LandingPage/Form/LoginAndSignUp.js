// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import styled from "styled-components";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";
import Divider from "material-ui/Divider";
import Button from "material-ui/Button";
import Card from "material-ui/Card";
// IMPORT icons
import FacebookIcon from "mdi-react/FacebookIcon";
import GoogleIcon from "mdi-react/GoogleIcon";
// IMPORT Redux Actions
import { signUp, logIn, getProfile } from "reducers/user/actions";
// IMPORT components
import CardHeaderTitleAndSubtitle from "components/UI/Card/Header/TitleAndSubtitle";
import PasswordWithToggleView from "components/UI/Input/PasswordWithToggleView";
import SocialNetworkChip from "components/UI/Chip/SocialNetwork";
import CardContent from "components/UI/Card/Content";
// IMPORT from same folder
import {
  LoginFormWrapper,
  ButtonWrapper,
  SocialNetworksWrapper,
  FormInputSpacing,
  FormOptionsWrapper
} from "./Wrapper";
// Custom Styles
const StyledCard = styled(Card)`
  width: ${props => props.theme.maxWidthSize.loginForm}px;
`;
// Component: Stateful
class LoginAndSignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      login: this.props.login,
      showPassword: false
    };
  }

  render() {
    const {
      title,
      subtitle,
      userLabel,
      userPlaceholder,
      forgotPassword,
      facebookText,
      googleText,
      footerText,
      termsOfUse,
      linkTo,
      linkToForgotPassword,
      isLoading,
      intl
    } = this.props;

    return (
      <LoginFormWrapper>
        <StyledCard>
          {isLoading && <LinearProgress color="secondary" />}
          <CardContent>
            {/* FORM HEADER */}
            <CardHeaderTitleAndSubtitle title={title} subtitle={subtitle} />

            {/* FORM INPUTS */}
            <form onSubmit={event => event.preventDefault()}>
              <FormInputSpacing>
                <TextField
                  disabled={isLoading}
                  id="user"
                  type="email"
                  fullWidth
                  onChange={this.handleChange("email")}
                  label={intl.formatMessage({ id: userLabel })}
                  placeholder={intl.formatMessage({ id: userPlaceholder })}
                />
              </FormInputSpacing>

              <FormInputSpacing>
                <PasswordWithToggleView
                  disabled={isLoading}
                  label="password-label"
                  placeholder={intl.formatMessage({
                    id: "password-placeholder"
                  })}
                  password={this.state.password}
                  handleChange={this.handleChange}
                />
              </FormInputSpacing>

              {/* FORM BUTTONS AND OPTIONS */}
              <ButtonWrapper>
                <Button
                  type="submit"
                  disabled={isLoading}
                  variant="raised"
                  color="primary"
                  onClick={() => this.loginOrSignUp(this.state, this.props)}
                >
                  <FormattedMessage id={title} />
                </Button>
                {forgotPassword && (
                  <Button
                    disabled={isLoading}
                    size="small"
                    color="primary"
                    onClick={linkToForgotPassword}
                  >
                    <FormattedMessage id={forgotPassword} />
                  </Button>
                )}
              </ButtonWrapper>
            </form>

            {termsOfUse && (
              <FormOptionsWrapper>
                <Typography variant="caption">
                  <FormattedMessage id={termsOfUse} />
                </Typography>
              </FormOptionsWrapper>
            )}

            <Divider />

            {/* SOCIAL NETWORK LOGIN OR SIGN UP */}
            {false && (
              <SocialNetworksWrapper>
                <SocialNetworkChip
                  buttonText={facebookText}
                  color="facebook"
                  icon={FacebookIcon}
                  handleClick={this.handleClick}
                />
                <SocialNetworkChip
                  buttonText={googleText}
                  color="googleRed"
                  icon={GoogleIcon}
                  handleClick={this.handleClick}
                />
              </SocialNetworksWrapper>
            )}
          </CardContent>

          {/* FORM FOOTER / ADDITIONAL OPTIONS */}
          <FormOptionsWrapper>
            <Button
              disabled={isLoading}
              size="small"
              color="primary"
              onClick={linkTo}
            >
              <FormattedMessage id={footerText} />
            </Button>
          </FormOptionsWrapper>
        </StyledCard>
      </LoginFormWrapper>
    );
  }

  loginOrSignUp = (state, props) => {
    const { email, password } = state;
    const { locale } = props;
    let body = {
      email,
      password,
      locale
    };
    if (state.login) {
      props.logIn(body);
    } else {
      props.signUp(body);
    }
  };

  handleClick = () => {
    // Handle Facebook & Google Log In or Sign Up
  };

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };
}
// PropTypes
LoginAndSignUpForm.propTypes = {
  login: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired,
  facebookText: PropTypes.string.isRequired,
  googleText: PropTypes.string.isRequired,
  footerText: PropTypes.string.isRequired,
  termsOfUse: PropTypes.string,
  forgotPassword: PropTypes.string,
  linkToForgotPassword: PropTypes.func,
  linkTo: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  locale: state.app.locale,
  isLoading: state.app.is_loading
});

const mapDispatchToProps = dispatch => ({
  logIn: body => dispatch(logIn(body)),
  signUp: body => dispatch(signUp(body)),
  getProfile: body => dispatch(getProfile(body))
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  injectIntl
)(LoginAndSignUpForm);
