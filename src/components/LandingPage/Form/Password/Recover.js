// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { injectIntl } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import TextField from "material-ui/TextField";
import Button from "material-ui/Button";
import Card from "material-ui/Card";
// IMPORT components
import CardHeaderTitleAndSubtitle from "components/UI/Card/Header/TitleAndSubtitle";
import CardContent from "components/UI/Card/Content";
// IMPORT from parent folder
import {
  FormInputSpacing,
  FormOptionsWrapper,
  LoginFormWrapper,
  ButtonWrapper
} from "../Wrapper";
// Custom Styles
const StyledCard = styled(Card)`
  width: ${props => props.theme.maxWidthSize.loginForm}px;
`;
// Component: Stateful
class LandingPageFormPasswordRecover extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      success: false
    };
  }

  render() {
    const { linkTo, intl } = this.props;

    return (
      <LoginFormWrapper>
        <StyledCard>
          <CardContent>
            {this.state.success ? (
              <CardHeaderTitleAndSubtitle
                title="forgot-password.sucess-title"
                subtitle="forgot-password.sucess-text"
              />
            ) : (
              <div>
                <CardHeaderTitleAndSubtitle
                  title="forgot-password"
                  subtitle="forgot-password.subtitle"
                />

                <FormInputSpacing>
                  <TextField
                    id="user"
                    type="email"
                    fullWidth
                    onChange={this.handleChange("email")}
                    label={intl.formatMessage({ id: "login-page.user" })}
                    placeholder={intl.formatMessage({
                      id: "login-page.user-placeholder"
                    })}
                  />
                </FormInputSpacing>
                {/* FORM BUTTONS AND OPTIONS */}
                <ButtonWrapper>
                  <Button
                    variant="raised"
                    color="primary"
                    onClick={this.handleClick}
                  >
                    <FormattedMessage id="forgot-password.recovery-password" />
                  </Button>
                </ButtonWrapper>
              </div>
            )}
          </CardContent>

          {/* FORM FOOTER / ADDITIONAL OPTIONS */}
          <FormOptionsWrapper>
            <Button size="small" color="primary" onClick={linkTo}>
              <FormattedMessage id="forgot-password.try-login" />
            </Button>
          </FormOptionsWrapper>
        </StyledCard>
      </LoginFormWrapper>
    );
  }

  handleChange = prop => event => {
    this.setState({ [prop]: event.target.value });
  };

  handleClick = () => {
    this.setState(prevState => ({
      success: !prevState.success
    }));
  };
}
// PropTypes
LandingPageFormPasswordRecover.propTypes = {
  linkTo: PropTypes.func.isRequired
};

export default injectIntl(LandingPageFormPasswordRecover);
