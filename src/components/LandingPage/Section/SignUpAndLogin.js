// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT components
import SignUpButton from "components/UI/Button/SignUpButton";
import { WhiteTypography } from "components/UI/Typography";
// IMPORT LandingPage components
import RecoverPasswordForm from "components/LandingPage/Form/Password/Recover";
import LoginAndSignUpForm from "components/LandingPage/Form/LoginAndSignUp";
// Custom Styles
const BackgroundImg = styled.div`
   {
    display: flex;
    background: url("${props => props.background}") no-repeat center center;
    background-size: cover;
    min-height: calc(${props => props.theme.heightDevicesSize.HD}px - ${props =>
  props.theme.headerSize.publicHeader}px);
  }
`;

const Content = styled.div`
   {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    background-color: rgba(0, 0, 0, 0.5);
    padding: ${props => props.theme.spacing.default}px 0;
  }
`;

const Title = styled.div`
  && {
    display: flex;
    justify-content: center;
    text-align: center;
    max-width: ${props => props.theme.maxWidthSize.landingTitle}px;
    margin-bottom: ${props => props.theme.spacing.xxxxl}px;
  }
`;

const Buttons = styled.div`
   {
    display: flex;
    justify-content: space-around;
    max-width: ${props => props.theme.maxWidthSize.landingTitle}px;
  }
`;

const FadeInLogin = styled.div`
   {
    opacity: ${props => (props.action === "login" ? 1 : 0)};
    transition: 0.5s;
  }
`;

const FadeInSignup = styled.div`
   {
    opacity: ${props => (props.action === "signup" ? 1 : 0)};
    transition: 0.5s;
  }
`;

const FadeInRecover = styled.div`
   {
    opacity: ${props => (props.action === "recover" ? 1 : 0)};
    transition: 0.5s;
  }
`;
// Component: Stateless
const LandingPageSectionSignUpAndLogin = ({
  background,
  action,
  clickFunction
}) => (
  <BackgroundImg background={background}>
    <Content>
      <Title>
        <WhiteTypography variant="display2">
          <FormattedMessage id="landing-page.join-the-best-place" />
        </WhiteTypography>
      </Title>
      <FadeInLogin action={action}>
        {action === "login" && (
          <LoginAndSignUpForm
            login={true}
            title="landing-page.log-in"
            subtitle="login-page.message"
            userLabel="login-page.user"
            userPlaceholder="login-page.user-placeholder"
            facebookText="login-page.facebook"
            googleText="login-page.google"
            footerText="login-page.dont-have-account"
            forgotPassword="landing-page.forgot-password"
            linkToForgotPassword={clickFunction("action", "recover")}
            linkTo={clickFunction("action", "signup")}
          />
        )}
      </FadeInLogin>
      <FadeInSignup action={action}>
        {action === "signup" && (
          <LoginAndSignUpForm
            login={false}
            title="landing-page.sign-up"
            subtitle="sign-up-page.message"
            userLabel="sign-up-page.user"
            userPlaceholder="sign-up-page.user-placeholder"
            facebookText="sign-up-page.facebook"
            googleText="sign-up-page.google"
            footerText="sign-up-page.already-have-account"
            termsOfUse="sign-up-page.terms-of-use"
            linkTo={clickFunction("action", "login")}
          />
        )}
      </FadeInSignup>
      <FadeInRecover action={action}>
        {action === "recover" && (
          <RecoverPasswordForm linkTo={clickFunction("action", "login")} />
        )}
      </FadeInRecover>
      {action === "" && (
        <Buttons>
          <SignUpButton onClick={clickFunction("action", "signup")}>
            <FormattedMessage id="landing-page.sign-up" />
          </SignUpButton>
          <SignUpButton onClick={clickFunction("action", "login")}>
            <FormattedMessage id="landing-page.log-in" />
          </SignUpButton>
        </Buttons>
      )}
    </Content>
  </BackgroundImg>
);
// PropTypes
LandingPageSectionSignUpAndLogin.propTypes = {
  background: PropTypes.string.isRequired,
  action: PropTypes.string.isRequired,
  clickFunction: PropTypes.func.isRequired
};

export default LandingPageSectionSignUpAndLogin;
