// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
import ReactSVG from "react-svg";
// IMPORT material
import Typography from "material-ui/Typography";
// Custom Styles
const Content = styled.div`
   {
    display: flex;
    justify-content: center;
    padding: ${props => props.theme.spacing.xxxxxl}px;
  }
`;

const InsideContent = styled.div`
   {
    display: flex;
    justify-content: space-around;
    width: ${props => props.theme.widthDevicesSize.xga}px;
  }
`;

const GamesSection = styled.div`
   {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

const Games = styled.div`
   {
    display: flex;
    justify-content: space-around;
  }
`;

const GameIcon = styled.div`
   {
    width: ${props => props.theme.iconSize.xxxl}px;
    fill: ${props => props.theme.text.primary};
    margin: ${props => props.theme.spacing.default}px
      ${props => props.theme.spacing.default}px 0
      ${props => props.theme.spacing.default}px;
  }
`;
// component: Stateless
const LandingPageGamesSection = props => (
  <Content>
    <InsideContent>
      <GamesSection>
        <Typography variant="body1">
          <FormattedMessage id="landing-page.supported-games" />
        </Typography>
        <Games>
          {props.supportedGames.map((content, index) => (
            <GameIcon key={`LandingPagesupportedGames ${index}`}>
              <ReactSVG path={content.icon} />
            </GameIcon>
          ))}
        </Games>
      </GamesSection>
      <GamesSection>
        <Typography variant="body1">
          <FormattedMessage id="landing-page.coming-soon" />
        </Typography>
        <Games>
          {props.commingSoonGames.map((content, index) => (
            <GameIcon key={`commingSoonGames ${index}`}>
              <ReactSVG path={content.icon} />
            </GameIcon>
          ))}
        </Games>
      </GamesSection>
    </InsideContent>
  </Content>
);
// PropTypes
LandingPageGamesSection.propTypes = {
  supportedGames: PropTypes.array.isRequired,
  commingSoonGames: PropTypes.array.isRequired
};

export default LandingPageGamesSection;
