// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT components
import SignUpButton from "components/UI/Button/SignUpButton";
import { WhiteTypography } from "components/UI/Typography";
// Custom Styles
const Background = styled.div`
   {
    display: flex;
    background-color: ${props => props.theme.background.purple};
    min-height: calc(
      2 * ${props => props.theme.minHeightSize.landingFeatureList}px / 3
    );
    padding: ${props => props.theme.spacing.default}px;
  }
`;

const Content = styled.div`
   {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

const ButtonWrapper = styled.div`
   {
    margin-top: calc(2 * ${props => props.theme.spacing.default}px);
  }
`;
// Component: Stateless
const LandingPageSignUpActionSection = props => (
  <Background>
    <Content>
      <WhiteTypography variant="headline">
        <FormattedMessage id="landing-page-release-date" />
      </WhiteTypography>
      <WhiteTypography variant="headline">
        <FormattedHTMLMessage id="landing-page-release-date-2" />
      </WhiteTypography>
      <ButtonWrapper>
        <SignUpButton onClick={props.clickFunction}>
          <FormattedMessage id="landing-page.sign-up-today" />
        </SignUpButton>
      </ButtonWrapper>
    </Content>
  </Background>
);
// PropTypes
LandingPageSignUpActionSection.propTypes = {
  clickFunction: PropTypes.func.isRequired
};

export default LandingPageSignUpActionSection;
