// IMPORT libs
import React from "react";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT components
import AvatarRowList from "components/UI/List/AvatarRowList";
// Custom Styles
const TeamContainer = styled.div`
   {
    display: flex;
    justify-content: center;
  }
`;

const TeamList = styled.div`
   {
    width: ${props => props.theme.widthDevicesSize.xga}px;
  }
`;
// Component: Stateless
const LandingPageTeamSection = props => (
  <TeamContainer>
    <TeamList>
      <AvatarRowList title="landing-page.team" cardsList={props.cardsList} />
    </TeamList>
  </TeamContainer>
);
// PropTypes
LandingPageTeamSection.propTypes = {
  cardsList: PropTypes.array.isRequired
};

export default LandingPageTeamSection;
