// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import { CardMedia } from "material-ui/Card";
// IMPORT components
import { WhiteTypography } from "components/UI/Typography";
// Custom Styles
const ItemBackground = styled.div`
   {
    display: flex;
    background: url("${props => props.background}") no-repeat center center;
    background-size: cover;
    min-height: ${props => props.theme.minHeightSize.landingFeatureList}px;
  }
`;

const Item = styled.div`
   {
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: rgba(0, 0, 0, 0.7);
  }
`;

const StyledMedia = styled(CardMedia)`
  && {
    height: ${props => props.theme.cardMediaSize.xxl}px;
    width: ${props => props.theme.cardMediaSize.xxxl}px;
    margin-right: calc(2 * ${props => props.theme.spacing.default}px);
    float: right;
  }
`;

const ContentMaxWidth = styled.div`
   {
    max-width: calc(${props => props.theme.widthDevicesSize.xga}px / 2);
  }
`;

const Title = styled.div`
   {
    margin-bottom: calc(2 * ${props => props.theme.spacing.default}px);
  }
`;
// Component: Stateless
const LandingPageFeaturesList = props => (
  <div>
    {props.list.map((content, index) => (
      <ItemBackground key={index} background={content.background}>
        <Item>
          <ContentMaxWidth>
            <StyledMedia image={content.image} />
          </ContentMaxWidth>
          <ContentMaxWidth>
            <Title>
              <WhiteTypography variant="display1">
                <FormattedMessage id={content.title} />
              </WhiteTypography>
            </Title>
            <WhiteTypography variant="subheading">
              <FormattedHTMLMessage id={content.text} />
            </WhiteTypography>
          </ContentMaxWidth>
        </Item>
      </ItemBackground>
    ))}
  </div>
);
// PropTypes
LandingPageFeaturesList.propTypes = {
  list: PropTypes.array.isRequired
};

export default LandingPageFeaturesList;
