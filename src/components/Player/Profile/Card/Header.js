// IMPORT libs
import React from "react";
// IMPORT material
import Card from "material-ui/Card";
// IMPORT from parent folder
import PlayerProfileHeader from "../Header/";
// Component: Stateless
const PlayerProfileCardAllProfileInfo = ({ userData }) => (
  <Card>
    <PlayerProfileHeader
      background_image={userData.background_image}
      avatar={userData.avatar}
      first_name={userData.first_name}
      display_name={userData.display_name}
      last_name={userData.last_name}
      headline={userData.headline}
      location={userData.location}
    />
  </Card>
);

export default PlayerProfileCardAllProfileInfo;
