// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
// IMPORT material
import { CardMedia } from "material-ui/Card";
// IMPORT icons
import CameraIcon from "mdi-react/CameraIcon";
// IMPORT Global contants
import CONSTANTS from "appConstants";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
import CardContent from "components/UI/Card/Content";
// IMPORT from parent folder
import {
  Content,
  AvatarPosition,
  FeatureImageChangeBackground,
  ClickToChangeBackground,
  AvatarChangeBackground
} from "components/UI/Wrapper/HeaderAllProfileInfo";
import {
  LocationTypography,
  TitleTypography,
  NameTypography
} from "components/UI/Typography/HeaderAllProfileInfo";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
   {
    height: ${props => props.theme.cardMediaSize.xl}px;
    width: 100%;
    position: relative;
    margin-bottom: calc(${props => props.theme.iconSize.xxxl}px / 2);
  }
`;

const StyledCardContent = styled(CardContent)`
  &&& {
    width: 100%;
    text-align: center;
    overflow: hidden;
  }
`;
// Component: Stateless
const PlayerProfileHeaderAllProfileInfo = ({
  background_image,
  avatar,
  first_name,
  display_name,
  last_name,
  headline,
  location,
  changeAvatar,
  changeBackground,
  inputAvatarId,
  inputBackgroundId
}) => (
  <Content>
    <StyledCardMedia
      image={background_image || CONSTANTS.default_backgorund_img}
    >
      {changeBackground && (
        <ClickToChangeBackground
          onClick={() => document.getElementById(inputBackgroundId).click()}
        >
          <FeatureImageChangeBackground>
            <DefaultIcon fill="bodyContrast" size="medium">
              <CameraIcon />
            </DefaultIcon>
          </FeatureImageChangeBackground>
        </ClickToChangeBackground>
      )}
      <AvatarPosition>
        {changeAvatar && (
          <AvatarChangeBackground
            onClick={() => document.getElementById(inputAvatarId).click()}
          >
            <DefaultIcon fill="bodyContrast" size="medium">
              <CameraIcon />
            </DefaultIcon>
          </AvatarChangeBackground>
        )}
        <CustomSizeCircleAvatar
          src={avatar || CONSTANTS.default_avatar_img}
          size="xxxl"
          id="signup-username-avatar"
        />
      </AvatarPosition>
    </StyledCardMedia>
    <StyledCardContent>
      <NameTypography variant="headline">
        {first_name} {display_name && <span>&ldquo;{display_name}&rdquo;</span>}{" "}
        {last_name}
      </NameTypography>
      <TitleTypography variant="subheading" noWrap>
        {headline}
      </TitleTypography>
      <LocationTypography variant="subheading" noWrap>
        {location}
      </LocationTypography>
    </StyledCardContent>
  </Content>
);

PlayerProfileHeaderAllProfileInfo.propTypes = {
  background_image: PropTypes.string,
  avatar: PropTypes.string,
  first_name: PropTypes.string,
  display_name: PropTypes.string,
  last_name: PropTypes.string,
  headline: PropTypes.string,
  location: PropTypes.string
};

export default PlayerProfileHeaderAllProfileInfo;
