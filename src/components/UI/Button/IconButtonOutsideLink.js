import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const IconWrapperMDI = styled.a`
  svg {
    fill: ${props =>
      props.secondaryText
        ? props.theme.text.secondary
        : props.theme.text.footer};
    width: ${props => props.theme.iconSize[props.iconSize]}px;
    height: ${props => props.theme.iconSize[props.iconSize]}px;
  }
`;

class IconButtonWithOutsideLink extends Component {
  render() {
    const { link, iconSize, secondaryText } = this.props;
    return (
      <IconWrapperMDI
        secondaryText={secondaryText}
        iconSize={iconSize}
        key={link}
        href={link}
        target="_blank"
        rel="noopener noreferrer"
      >
        <this.props.icon />
      </IconWrapperMDI>
    );
  }
}

IconButtonWithOutsideLink.propTypes = {
  link: PropTypes.string.isRequired,
  icon: PropTypes.func.isRequired,
  iconSize: PropTypes.oneOf(["small", "medium", "large", "xl"]).isRequired,
  secondaryText: PropTypes.bool
};

export default IconButtonWithOutsideLink;
