import Button from "material-ui/Button";
import styled from "styled-components";

const SignUpButton = styled(Button)`
  &&& {
    background: ${props => props.theme.misc.signUpButton};
    color: ${props => props.theme.text.darkGrey};
    font-size: 1.35em;
    text-transform: none;

    margin: 0 ${props => props.theme.spacing.xxl}px;
    padding-left: ${props => props.theme.spacing.xxxl}px;
    padding-right: ${props => props.theme.spacing.xxxl}px;
`;

export default SignUpButton;
