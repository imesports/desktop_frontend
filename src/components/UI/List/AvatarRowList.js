import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import { FormattedMessage } from "react-intl";

import Typography from "material-ui/Typography";
import List from "material-ui/List";

import AvatarWithText from "../Avatar/AvatarWithText";

const BetweenCardsTextWrapper = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px;
  }
`;

class AvatarRowList extends Component {
  render() {
    const { title, cardsList } = this.props;
    return (
      <div>
        <BetweenCardsTextWrapper>
          <Typography>
            <FormattedMessage id={title} />
          </Typography>
        </BetweenCardsTextWrapper>
        <List>
          {cardsList.map(content => (
            <AvatarWithText
              key={content.listItemTitle}
              listItemTitle={content.listItemTitle}
              listItemSubTitle={content.listItemSubTitle}
              listItemImage={content.listItemImage}
              linkedInUrl={content.linkedInUrl}
            />
          ))}
        </List>
      </div>
    );
  }
}

AvatarRowList.propTypes = {
  title: PropTypes.string.isRequired,
  cardsList: PropTypes.array.isRequired
};

export default AvatarRowList;
