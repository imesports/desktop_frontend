// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Button from "material-ui/Button";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";

const TooltipDialog = ({ open, title, text, buttonText, closeAction }) => (
  <div>
    <Dialog open={open} onClose={() => closeAction(false)}>
      <DialogTitle>
        <FormattedMessage id={title} />
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <FormattedMessage id={text} />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => closeAction(false)} color="primary" autoFocus>
          <FormattedMessage id={buttonText} />
        </Button>
      </DialogActions>
    </Dialog>
  </div>
);

TooltipDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  closeAction: PropTypes.func.isRequired
};

export default TooltipDialog;
