import IconButton from "material-ui/IconButton";
import styled from "styled-components";

export const DefaultIcon = styled(IconButton)`
  &&& {
    height: calc(
      ${props => props.theme.iconSize[props.size]}px +
        ${props => props.theme.spacing.small}px
    );
    width: calc(
      ${props => props.theme.iconSize[props.size]}px +
        ${props => props.theme.spacing.small}px
    );
  }
  &&& svg {
    fill: ${props => props.theme.icon[props.fill]};
    height: ${props => props.theme.iconSize[props.size]}px;
    width: ${props => props.theme.iconSize[props.size]}px;
  }
`;

export const NoButtonIcon = styled.div`
  &&& {
    height: ${props => props.theme.iconSize[props.size]}px;
    width: ${props => props.theme.iconSize[props.size]}px;
  }
  &&& svg {
    fill: ${props => props.theme.icon[props.fill]};
    height: ${props => props.theme.iconSize[props.size]}px;
    width: ${props => props.theme.iconSize[props.size]}px;
  }
`;
