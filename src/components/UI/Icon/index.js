import IconButton from "material-ui/IconButton";
import styled from "styled-components";

export const ThemedIconButton = styled(IconButton)`
  &&& svg {
    fill: ${props => props.theme.icon.default};
  }
`;
