import styled from "styled-components";

const CardContent = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px;
    width: 100%;
  }
`;

export default CardContent;
