// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import styled from "styled-components";
// IMPORT material
import Typography from "material-ui/Typography";
// Custom Styles
const CardTitle = styled.div`
  margin-bottom: ${props => props.theme.spacing.default}px;
`;
// Component: Stateless
const CardHeaderTitleAndSubtitle = ({ title, subtitle }) => (
  <CardTitle>
    <Typography variant="headline">
      <FormattedMessage id={title} />
    </Typography>
    <Typography variant="body1">
      <FormattedHTMLMessage id={subtitle} />
    </Typography>
  </CardTitle>
);

export default CardHeaderTitleAndSubtitle;
