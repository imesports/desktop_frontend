// IMPORT libs
import React from "react";
import styled from "styled-components";
import { connect } from "react-redux";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
// Custom Styles
const Content = styled.div`
   {
    padding: ${props => props.theme.spacing.small}px;
    display: flex;
    align-items: center;
  }
`;

const TextContent = styled.div`
   {
    padding-left: ${props => props.theme.spacing.small}px;
    display: flex;
    flex-direction: column;
    max-width: calc(100% - ${props => props.theme.iconSize.large}px);
  }
`;
// Component: Stateless
const CardHeaderUserAvatarNameEmail = props => (
  <Content>
    <CustomSizeCircleAvatar size="large" src={props.userProfile.thumbnail} />
    <TextContent>
      <Typography variant="body2" noWrap>
        {props.userProfile.first_name}{" "}
        {props.userProfile.display_name && (
          <span>&ldquo;{props.userProfile.display_name}&rdquo;</span>
        )}{" "}
        {props.userProfile.last_name}{" "}
      </Typography>
      <Typography variant="caption">{props.userProfile.email}</Typography>
    </TextContent>
  </Content>
);
// Redux Store / State
const mapStateToProps = state => ({
  userProfile: state.user.profile
});

export default connect(mapStateToProps)(CardHeaderUserAvatarNameEmail);
