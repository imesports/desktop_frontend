import styled from "styled-components";
import Divider from "material-ui/Divider";

export const Content = styled.div`
   {
    padding-top: ${props => props.theme.spacing.xxl}px;
  }
`;

export const InputContent = styled.div`
   {
    width: 100%;
    display: flex;
    align-items: center;
    flex-wrap: wrap;
  }
`;

export const InputTag = styled.div`
   {
    width: 20%;
  }
`;

export const InputField = styled.div`
   {
    width: 75%;
  }
`;

export const TooltipField = styled.div`
   {
    width: 5%;
  }
`;

export const DividerContent = styled.div`
   {
    display: flex;
    justify-content: flex-end;
  }
`;

export const StyledDivider = styled(Divider)`
  && {
    margin-top: ${props => props.theme.spacing.xxl}px;
    width: 80%;
  }
`;
