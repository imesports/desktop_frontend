import styled from "styled-components";

export const PublicHeaderWrapper = styled.div`
   {
    margin-top: ${props => props.theme.headerSize.publicHeader}px;
  }
`;

export const MaxWidthSignUpWrapper = styled.div`
   {
    display: flex;
    justify-content: center;
    max-width: ${props => props.theme.widthDevicesSize.xga}px;
    padding: 0 ${props => props.theme.spacing.xxl}px;
    margin: 0 auto;
  }
`;

export const DefaultMarginTopWrapper = styled.div`
   {
    margin-top: ${props => props.theme.spacing.default}px;
  }
`;

export const LoggedInWrapper = styled.div`{
  display: flex;
  justify-content: center;

  margin-left: ${props => (props.drawer ? props.theme.menuSize.default : 0)}px;
  margin-left: ${props => props.noLeftMargin && 0}px;
  margin-top: ${props => props.theme.headerSize.commonToolbar}px;
  transition: 0.3s;

  @media (max-width: ${props => props.theme.breakingpoints.menuBreak}px) {
    margin-left: 0px;
}`;

export const MainContainerWrapper = styled.div`
   {
    margin-top: ${props => props.theme.spacing.default}px;
    min-width: ${props => props.theme.contentSize.default}px;
    width: ${props => props.theme.contentSize.default}px;
    display: flex;
    align-items: flex-start;
  }
`;

export const Column1of2 = styled.div`
   {
    min-width: ${props => props.theme.contentSize.columnOneOfTwo}px;
    width: ${props => props.theme.contentSize.columnOneOfTwo}px;
    margin-right: ${props => props.theme.contentSize.defaultMargin}px;
  }
`;

export const Column2of2 = styled.div`
   {
    min-width: ${props => props.theme.contentSize.columnTwoOfTwo}px;
    width: ${props => props.theme.contentSize.columnTwoOfTwo}px;
    margin-left: ${props => props.theme.contentSize.defaultMargin}px;
  }
`;
