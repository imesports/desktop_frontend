import styled from "styled-components";

const SocialNetworksWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  width: ${props => props.theme.maxWidthSize.landingTitle}px;
`;

export default SocialNetworksWrapper;
