import styled from "styled-components";

const CardWrapper = styled.div`
   {
    margin-bottom: ${props => props.theme.spacing.default}px;
    width: 100%;
  }
`;

export default CardWrapper;
