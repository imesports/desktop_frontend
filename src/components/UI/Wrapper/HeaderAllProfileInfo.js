import styled from "styled-components";

export const Content = styled.div`
   {
    width: 100%;
  }
`;

export const AvatarPosition = styled.div`
   {
    position: absolute;
    bottom: calc(-${props => props.theme.iconSize.xxxl}px / 2);
    left: calc(50% - ${props => props.theme.iconSize.xxxl}px / 2);
    z-index: 3;
  }
`;

export const FeatureImageChangeBackground = styled.div`
   {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: ${props => props.theme.background.body}90;
    z-index: 2;
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
    padding: ${props => props.theme.spacing.small}px;
  }
`;

export const ClickToChangeBackground = styled.div`
   {
    position: absolute;
    width: 100%;
    height: 100%;
    display: flex;
    z-index: 2;
    align-items: flex-end;
    justify-content: flex-end;
  }
`;

export const AvatarChangeBackground = styled.div`
   {
    border-radius: ${props => props.theme.iconSize.xxxl}px;
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: ${props => props.theme.background.body}90;
    z-index: 4;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
