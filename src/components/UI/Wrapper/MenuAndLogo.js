import styled from "styled-components";

const MenuAndLogo = styled.div`
   {
    min-width: calc(
      ${props => props.theme.menuSize.default}px -
        ${props => props.theme.spacing.default}px
    );
    display: flex;
    align-items: center;
    justify-content: flex-start;
  }
`;

export default MenuAndLogo;
