import styled from "styled-components";

export const PublicLogoWrapper = styled.div`
   {
    display: flex;
    align-items: center;
    justify-content: center;
    flex: 1;
    height: ${props => props.theme.headerSize.publicHeader}px;
  }
`;

export const LoggedLogoWrapper = styled.div`
   {
    margin: 0 ${props => props.theme.spacing.default}px;
  }
`;
