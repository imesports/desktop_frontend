// IMPORT libs
import React from "react";
import styled from "styled-components";
// IMPORT material
import { CircularProgress } from "material-ui/Progress";
// Custom styles
const Background = styled.div`
   {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: ${props => props.theme.background.body}90;
  }
`;

const CircularProgressPosition = styled.div`
   {
    position: fixed;
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 10000;
    top: 0;
    left: 0;
  }
`;
// Component: Stateless
const LoadingFullScreen = props => (
  <CircularProgressPosition>
    <Background />
    <CircularProgress style={{ zIndex: 10001 }} size={50} color="primary" />
  </CircularProgressPosition>
);

export default LoadingFullScreen;
