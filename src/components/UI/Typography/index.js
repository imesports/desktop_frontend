import Typography from "material-ui/Typography";
import styled from "styled-components";

export const WhiteTypography = styled(Typography)`
  && {
    color: ${props => props.theme.text.white};
  }
`;
