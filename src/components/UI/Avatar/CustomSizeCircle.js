import Avatar from "material-ui/Avatar";
import styled from "styled-components";

const CustomSizeCircleAvatar = styled(Avatar)`
  && {
    width: ${props => props.theme.iconSize[props.size]}px;
    height ${props => props.theme.iconSize[props.size]}px;
  }
`;

export default CustomSizeCircleAvatar;
