// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import { ListItem, ListItemText } from "material-ui/List";
import Typography from "material-ui/Typography";
// IMPORT icons
import LinkedinBoxIcon from "mdi-react/LinkedinBoxIcon";
// IMPORT components
import IconButtonOutsideLink from "components/UI/Button/IconButtonOutsideLink";
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
// Compponent: Stateless
const AvatarWithText = ({
  listItemTitle,
  listItemSubTitle,
  listItemImage,
  linkedInUrl
}) => (
  <ListItem>
    <CustomSizeCircleAvatar size="xxl" src={listItemImage} />
    <ListItemText
      disableTypography
      primary={
        <Typography variant="headline">
          <FormattedMessage id={listItemTitle} />
        </Typography>
      }
      secondary={
        <Typography variant="subheading">
          <FormattedMessage id={listItemSubTitle} />
        </Typography>
      }
    />
    {linkedInUrl && (
      <IconButtonOutsideLink
        secondaryText
        link={linkedInUrl}
        icon={LinkedinBoxIcon}
        iconSize="large"
      />
    )}
  </ListItem>
);
// PropTypes
AvatarWithText.propTypes = {
  listItemTitle: PropTypes.string.isRequired,
  listItemSubTitle: PropTypes.string.isRequired,
  listItemImage: PropTypes.string.isRequired,
  linkedInUrl: PropTypes.string
};

export default AvatarWithText;
