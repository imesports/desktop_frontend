import styled from "styled-components";

const LogoImg = styled.div`
   {
    fill: ${props => props.theme.text[props.color]};
    width: ${props => props.theme.logoSize[props.size]}px;
  }
`;

export default LogoImg;
