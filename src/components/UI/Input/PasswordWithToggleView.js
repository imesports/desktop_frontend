import React, { Component } from "react";
import PropTypes from "prop-types";

import { FormattedMessage } from "react-intl";

import Input, { InputLabel, InputAdornment } from "material-ui/Input";
import { FormControl } from "material-ui/Form";

import EyeOffIcon from "mdi-react/EyeOffIcon";
import EyeIcon from "mdi-react/EyeIcon";

import { ThemedIconButton } from "components/UI/Icon";

class PasswordWithToggleView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: false
    };
  }
  render() {
    const {
      label,
      placeholder,
      password,
      handleChange,
      disabled = false
    } = this.props;
    const { showPassword } = this.state;

    return (
      <FormControl fullWidth>
        <InputLabel htmlFor="password">
          <FormattedMessage id={label} />
        </InputLabel>
        <Input
          disabled={disabled}
          id="password"
          type={showPassword ? "text" : "password"}
          placeholder={placeholder}
          value={password}
          onChange={handleChange("password")}
          endAdornment={
            <InputAdornment position="end">
              <ThemedIconButton
                onClick={this.handleClickShowPasssword}
                onMouseDown={this.handleMouseDownPassword}
              >
                {showPassword ? <EyeOffIcon /> : <EyeIcon />}
              </ThemedIconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    );
  }

  handleMouseDownPassword = event => {
    event.preventDefault();
  };

  handleClickShowPasssword = () => {
    this.setState({ showPassword: !this.state.showPassword });
  };
}

PasswordWithToggleView.propTypes = {
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default PasswordWithToggleView;
