import React from "react";
import styled from "styled-components";
import { injectIntl } from "react-intl";

import MagnifyIcon from "mdi-react/MagnifyIcon";
import { NoButtonIcon } from "../Icon/IconFormatter";

const Content = styled.div`
   {
    display: flex;
    align-items: center;
  }
`;

const InputContent = styled.div`
   {
    display: flex;
    align-items: center;
    width: 500px;
    border: 1px solid;
    border-color: ${props => props.theme.border.default};
    border-right: none;
    padding: 2px 6px;
    border-radius: 2px 0 0 2px;
    height: 30px;
    @media (max-width: 1100px) {
      width: 400px;
    }
  }
`;

const StyledInput = styled.input`
   {
    border: none;
    padding: 0px;
    margin: 0px;
    height: auto;
    width: 100%;
    outline: none;
    font-family: "Roboto", "Helvetica", sans-serif;
    font-size: 16px;
    font-weight: 400;
    line-height: 24px;
    background-color: transparent;
    color: ${props => props.theme.text.primary};
  }
`;

const SearchButton = styled.button`
   {
    padding: 1px 16px;
    height: 30px;
    background-color: ${props => props.theme.border.light};
    border: 1px solid ${props => props.theme.border.default};
    border-radius: 0 2px 2px 0;

    :hover {
      background-color: ${props => props.theme.border.default};
      cursor: pointer;
    }

    :focus {
      outline: none;
    }
  }
`;

const SearchInput = props => (
  <Content>
    <InputContent>
      <StyledInput placeholder={props.intl.formatMessage({ id: "search" })} />
    </InputContent>
    <SearchButton>
      <NoButtonIcon fill="default" size="small">
        <MagnifyIcon />
      </NoButtonIcon>
    </SearchButton>
  </Content>
);

export default injectIntl(SearchInput);
