import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage, injectIntl } from "react-intl";

import MenuItem from "material-ui/Menu/MenuItem";
import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";

import {
  Content,
  InputContent,
  InputTag,
  InputField,
  DividerContent,
  StyledDivider
} from "components/UI/Wrapper/Input";

const DesktopSelectInput = props => {
  const {
    placeholder,
    label,
    name,
    id,
    required = false,
    value,
    onChange,
    disabled = false,
    list,
    intl
  } = props;

  return (
    <Content>
      <InputContent>
        <InputTag>
          <Typography variant="subheading">
            <FormattedMessage id={label} />
            {required && "*"}
          </Typography>
        </InputTag>
        <InputField>
          <TextField
            select
            fullWidth
            label={placeholder && intl.formatMessage({ id: placeholder })}
            name={name}
            id={id}
            required={required}
            value={value}
            onChange={onChange}
            disabled={disabled}
            inputProps={{ maxLength: props.maxLength }}
          >
            {list ? (
              list.map(option => (
                <MenuItem
                  key={`DesktopSelectInput ${props.label} ${option.id}`}
                  value={option.id}
                >
                  <FormattedMessage id={option.name} />
                </MenuItem>
              ))
            ) : (
              <MenuItem />
            )}
          </TextField>
        </InputField>
      </InputContent>
      <DividerContent>
        <StyledDivider />
      </DividerContent>
    </Content>
  );
};

DesktopSelectInput.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  disabled: PropTypes.bool,
  name: PropTypes.string,
  list: PropTypes.array
};

export default injectIntl(DesktopSelectInput);
