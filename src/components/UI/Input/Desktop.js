import React, { Component } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";

import Typography from "material-ui/Typography";
import TextField from "material-ui/TextField";

import { DefaultIcon } from "components/UI/Icon/IconFormatter";
import TooltipDialog from "components/UI/Dialog/Tooltip";
import {
  Content,
  InputContent,
  InputTag,
  InputField,
  TooltipField,
  DividerContent,
  StyledDivider
} from "components/UI/Wrapper/Input";

import InformationOutlineIcon from "mdi-react/InformationOutlineIcon";

class DesktopInput extends Component {
  state = {
    dialogOpen: false
  };

  render() {
    const {
      label,
      name,
      id,
      required = false,
      value,
      onChange,
      disabled = false,
      maxLength,
      tooltip = false,
      tooltipTitle,
      tooltipText,
      tooltipButtonText
    } = this.props;

    return (
      <Content>
        <InputContent>
          <InputTag>
            <Typography variant="subheading">
              <FormattedMessage id={label} />
              {required && "*"}
            </Typography>
          </InputTag>
          <InputField>
            <TextField
              name={name}
              id={id}
              required={required}
              fullWidth
              value={value}
              onChange={onChange}
              disabled={disabled}
              inputProps={{ maxLength: maxLength }}
            />
          </InputField>
          {tooltip && (
            <TooltipField>
              <DefaultIcon
                fill="primary"
                size="sm"
                onClick={() => this.openDialog(true)}
              >
                <InformationOutlineIcon />
              </DefaultIcon>

              <TooltipDialog
                open={this.state.dialogOpen}
                title={tooltipTitle}
                text={tooltipText}
                buttonText={tooltipButtonText}
                closeAction={this.openDialog}
              />
            </TooltipField>
          )}
        </InputContent>
        <DividerContent>
          <StyledDivider />
        </DividerContent>
      </Content>
    );
  }

  openDialog = value => {
    this.setState({ dialogOpen: value });
  };
}

DesktopInput.propTypes = {
  label: PropTypes.string.isRequired,
  required: PropTypes.bool,
  name: PropTypes.string,
  id: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
  maxLength: PropTypes.string
};

export default DesktopInput;
