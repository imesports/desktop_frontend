import Toolbar from "material-ui/Toolbar";
import AppBar from "material-ui/AppBar";
import styled from "styled-components";

export const ThemedAppBar = styled(AppBar)`
  && {
    background: ${props => props.theme.background.appbar};
    box-shadow: 0px 2px 4px -1px rgba(0, 0, 0, 0.2);
    top: 0;
    left: 0;
    width: 100%;
    min-width: 1000px;
    min-height: ${props => props.theme.headerSize.commonToolbar}px;
    height: ${props => props.theme.headerSize.commonToolbar}px;
    z-index: 101;
  }
`;

export const ThemedToolbar = styled(Toolbar)`
  && {
    padding: 0px ${props => props.theme.spacing.default}px;
    display: flex;
    align-items: center;
    width: 100%;
    min-height: ${props => props.theme.headerSize.commonToolbar}px;
    height: ${props => props.theme.headerSize.commonToolbar}px;
  }
`;
