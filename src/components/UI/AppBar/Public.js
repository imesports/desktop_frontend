import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import styled from "styled-components";

export const ThemedAppBar = styled(AppBar)`
  && {
    background: ${props => props.theme.background.appbar};
    width: 100%;
  }
`;

export const ThemedToolbar = styled(Toolbar)`
  && {
    padding: 0px ${props => props.theme.spacing.xs}px;
    display: flex;
    justify-content: center;
    width: 100%;
  }
`;

export const IconDiv = styled.div`
   {
    min-width: ${props => props.theme.iconSize.default}px;
  }
`;
