// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import ChevronRightIcon from "mdi-react/ChevronRightIcon";
// IMPORT components
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";
// Import Styles
import { MenuItemWrapper, TextWrapper } from "./Wrapper";
// Component: Stateless
const DefaultDrawerItem = props => {
  const {
    goTo,
    largePadding = false,
    formattedText,
    text,
    onClick,
    chevronRight
  } = props;
  const { pathname } = props.router.location;

  return (
    <div>
      {goTo ? (
        <Link to={goTo}>
          <MenuItemWrapper onClick={onClick} largePadding={largePadding}>
            <NoButtonIcon
              size="sm"
              fill={goTo === pathname ? "primary" : "default"}
            >
              {props.icon && <props.icon />}
              {props.svgIcon && props.svgIcon}
            </NoButtonIcon>
            <TextWrapper>
              <Typography
                variant={goTo === pathname ? "body2" : "body1"}
                noWrap
              >
                {formattedText && <FormattedMessage id={formattedText} />}
                {text}
              </Typography>
              {chevronRight && (
                <NoButtonIcon size="sm" fill="default">
                  <ChevronRightIcon />
                </NoButtonIcon>
              )}
            </TextWrapper>
          </MenuItemWrapper>
        </Link>
      ) : (
        <MenuItemWrapper onClick={onClick}>
          <NoButtonIcon size="sm" fill="default">
            {props.icon && <props.icon />}
            {props.svgIcon && props.svgIcon}
          </NoButtonIcon>
          <TextWrapper>
            <Typography variant="body1" noWrap>
              {formattedText && <FormattedMessage id={formattedText} />}
              {text}
            </Typography>
            {chevronRight && (
              <NoButtonIcon size="sm" fill="default">
                <ChevronRightIcon />
              </NoButtonIcon>
            )}
          </TextWrapper>
        </MenuItemWrapper>
      )}
    </div>
  );
};
// PropTypes
DefaultDrawerItem.propTypes = {
  onClick: PropTypes.func,
  formattedText: PropTypes.string,
  svgIcon: PropTypes.object,
  text: PropTypes.string,
  icon: PropTypes.func,
  goTo: PropTypes.string
};
// Redux Store / State
const mapStateToProps = state => ({
  router: state.router
});

export default connect(mapStateToProps)(DefaultDrawerItem);
