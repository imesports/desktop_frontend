import styled from "styled-components";

export const MenuItemWrapper = styled.div`
   {
    display: flex;
    width: 100%;
    align-items: center;

    padding: ${props => props.theme.spacing.xs}px
      ${props => props.theme.spacing.largeDefault}px;

    :hover {
      cursor: pointer;
      background-color: ${props => props.theme.background.hover};
    }
  }
`;

export const TextWrapper = styled.div`
   {
    width: calc(100% - ${props => props.theme.iconSize.sm}px);
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-left: ${props => props.theme.spacing.large}px;
  }
`;

export const TeamTextWrapper = styled.div`
   {
    width: calc(100% - 2 * ${props => props.theme.iconSize.sm}px);
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding-left: ${props => props.theme.spacing.large}px;
  }
`;
