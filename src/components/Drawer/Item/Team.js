// IMPORT libs
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT constants
import CONSTANTS from "appConstants";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";
// Import Styles
import { MenuItemWrapper, TeamTextWrapper } from "./Wrapper";
// Component: Stateless
const DefaultDrawerItem = props => {
  const { goTo, largePadding = false, name, avatar, svgIcon } = props;
  const { pathname } = props.router.location;

  return (
    <Link to={goTo}>
      <MenuItemWrapper largePadding={largePadding}>
        <CustomSizeCircleAvatar
          src={avatar || CONSTANTS.default_avatar_img}
          size="sm"
        />
        <TeamTextWrapper>
          <Typography variant={goTo === pathname ? "body2" : "body1"} noWrap>
            {name}
          </Typography>
        </TeamTextWrapper>
        <NoButtonIcon size="sm" fill="default">
          {svgIcon && svgIcon}
        </NoButtonIcon>
      </MenuItemWrapper>
    </Link>
  );
};

// PropTypes
DefaultDrawerItem.propTypes = {
  avatar: PropTypes.string,
  largePadding: PropTypes.bool,
  svgIcon: PropTypes.object,
  name: PropTypes.string.isRequired,
  goTo: PropTypes.string.isRequired
};
// Redux Store / State
const mapStateToProps = state => ({
  router: state.router
});

export default connect(mapStateToProps)(DefaultDrawerItem);
