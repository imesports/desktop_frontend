import styled from "styled-components";

export const DrawerContentWrapper = styled.div`
   {
    position: relative;
    width: ${props => props.theme.menuSize.default}px;
    min-width: ${props => props.theme.menuSize.default}px;
  }
`;

export const DrawerListContent = styled.div`
   {
    padding: ${props => props.theme.spacing.smallDefault}px 0;
  }
`;

export const NoContentWrapper = styled.div`
   {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    padding: ${props => props.theme.spacing.xs}px
      ${props => props.theme.spacing.largeDefault}px;
  }
`;
