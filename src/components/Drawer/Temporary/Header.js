// IMPORT libs
import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import ReactSVG from "react-svg";
// IMPORT icons
import MenuIcon from "mdi-react/MenuIcon";
// IMPORT components
import { LoggedLogoWrapper } from "components/UI/Wrapper/Logo";
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
import MenuAndLogo from "components/UI/Wrapper/MenuAndLogo";
import LogoImg from "components/UI/Img/Logo";
// IMPORT from same folder
import { ThemedAppBar, ThemedToolbar } from "./AppBar";
// IMPORT images
import Logo from "imgs/logo.svg";
// Component: Stateless
const TemporaryDrawerHeader = props => (
  <ThemedAppBar position="static">
    <ThemedToolbar>
      <MenuAndLogo>
        <DefaultIcon
          fill="default"
          onClick={() => props.toggleMenu("temporaryDrawer", false)}
        >
          <MenuIcon />
        </DefaultIcon>
        <LoggedLogoWrapper>
          <Link to="/home">
            <LogoImg color="primary" size="loggedIn">
              <ReactSVG path={Logo} />
            </LogoImg>
          </Link>
        </LoggedLogoWrapper>
      </MenuAndLogo>
    </ThemedToolbar>
  </ThemedAppBar>
);
// PropTypes
TemporaryDrawerHeader.propTypes = {
  toggleMenu: PropTypes.func.isRequired
};

export default TemporaryDrawerHeader;
