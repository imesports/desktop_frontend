import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import styled from "styled-components";

export const ThemedAppBar = styled(AppBar)`
  && {
    background: ${props => props.theme.background.appbar};
    top: 0;
    left: 0;
    width: ${props => props.theme.menuSize.default}px;
    min-width: ${props => props.theme.menuSize.default}px;
    height: ${props => props.theme.headerSize.commonToolbar}px;
    min-height: ${props => props.theme.headerSize.commonToolbar}px;
  }
`;

export const ThemedToolbar = styled(Toolbar)`
  && {
    padding: 0px ${props => props.theme.spacing.default}px;
    display: flex;
    align-items: center;
    width: 100%;
    min-height: ${props => props.theme.headerSize.commonToolbar}px;
    height: ${props => props.theme.headerSize.commonToolbar}px;
  }
`;
