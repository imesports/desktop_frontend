// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
// IMPORT material
import Hidden from "material-ui/Hidden";
// IMPORT components
import TemporaryDrawerHeader from "components/Drawer/Temporary/Header";
import {
  PersistentDrawer,
  TemporaryDrawer
} from "components/Drawer/StyledDrawer";
// IMPORT from same folder
import UserDrawerContent from "./Content";
// Component: Stateless
const DefaultDrawer = props => (
  <div>
    <Hidden mdDown>
      <PersistentDrawer open={props.persistentDrawer}>
        <UserDrawerContent />
      </PersistentDrawer>
    </Hidden>
    <Hidden lgUp>
      <TemporaryDrawer
        open={props.temporaryDrawer}
        onClose={() => props.toggleMenu("temporaryDrawer", false)}
      >
        <TemporaryDrawerHeader toggleMenu={props.toggleMenu} />
        <UserDrawerContent toggleMenu={props.toggleMenu} />
      </TemporaryDrawer>
    </Hidden>
  </div>
);
// PropTypes
DefaultDrawer.propTypes = {
  persistentDrawer: PropTypes.bool,
  temporaryDrawer: PropTypes.bool,
  toggleMenu: PropTypes.func.isRequired
};

export default DefaultDrawer;
