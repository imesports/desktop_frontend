// IMPORT libs
import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT material
import Divider from "material-ui/Divider";
// IMPORT icons
import AccountBoxIcon from "mdi-react/AccountBoxIcon";
// import NewspaperIcon from "mdi-react/NewspaperIcon";
import HomeIcon from "mdi-react/HomeIcon";
// IMPORT components
import HeaderUserAvatarNameEmail from "components/UI/Card/Header/UserAvatarNameEmail";
// IMPORT from parent folder
import { DrawerContentWrapper, DrawerListContent } from "../Wrapper";
import DefaultDrawerItem from "../Item/Default";
import DrawerFooter from "../Footer";
// IMPORT from same folder
// import UserDrawerInitalizationList from "./List/InitalizationList";
// import UserDrawerDefaultList from "./List/Default";
import UserDrawerTeamsList from "./List/Teams";
import UserDrawerGamesList from "./List/Games";
// Components: Stateless
const UserDrawerContent = props => {
  const closeTemporaryDrawer = () => {
    props.toggleMenu && props.toggleMenu("temporaryDrawer", false);
  };
  return (
    <DrawerContentWrapper>
      <HeaderUserAvatarNameEmail />
      <Divider />
      <DrawerListContent>
        <DefaultDrawerItem
          formattedText="home"
          icon={HomeIcon}
          goTo="/home"
          largePadding
          onClick={() => closeTemporaryDrawer()}
        />
        <DefaultDrawerItem
          formattedText="my-profile"
          icon={AccountBoxIcon}
          goTo={`/player/${props.userProfile.url_handle}`}
          largePadding
          onClick={() => closeTemporaryDrawer()}
        />
        {/*<DefaultDrawerItem
          formattedText="news"
          icon={NewspaperIcon}
          goTo="/news"
          largePadding
          onClick={() => closeTemporaryDrawer()}
        />*/}
      </DrawerListContent>
      <Divider />
      <UserDrawerTeamsList onClick={() => closeTemporaryDrawer()} />
      <UserDrawerGamesList onClick={() => closeTemporaryDrawer()} />
      {/*<UserDrawerDefaultList
        onClick={() => closeTemporaryDrawer()}
        list={UserDrawerInitalizationList.championshipList}
      />
      <UserDrawerDefaultList
        onClick={() => closeTemporaryDrawer()}
        list={UserDrawerInitalizationList.utilList}
      />*/}
      <DrawerFooter />
    </DrawerContentWrapper>
  );
};
// PropTypes
UserDrawerContent.propTypes = {
  toggleMenu: PropTypes.func
};
// Redux Store / State
const mapStateToProps = state => ({
  userProfile: state.user.profile
});

export default connect(mapStateToProps)(UserDrawerContent);
