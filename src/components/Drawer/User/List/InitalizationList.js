import MessageAlertIcon from "mdi-react/MessageAlertIcon";
import HelpCircleIcon from "mdi-react/HelpCircleIcon";
import SettingsIcon from "mdi-react/SettingsIcon";
import LadybugIcon from "mdi-react/LadybugIcon";
import TrophyIcon from "mdi-react/TrophyIcon";

const UserDrawerInitalizationList = {
  championshipList: [
    {
      text: "championships",
      icon: TrophyIcon,
      goTo: "/championships"
    }
  ],
  utilList: [
    {
      text: "settings",
      icon: SettingsIcon,
      goTo: "/settings"
    },
    {
      text: "help",
      icon: HelpCircleIcon,
      goTo: "/help"
    },
    {
      text: "report-bug",
      icon: LadybugIcon,
      goTo: "/bugreport"
    },
    {
      text: "send-feedback",
      icon: MessageAlertIcon,
      goTo: "/feedback"
    }
  ]
};

export default UserDrawerInitalizationList;
