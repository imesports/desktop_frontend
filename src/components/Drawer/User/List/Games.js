// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
// IMPORT material
import { CircularProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
// IMPORT components
import { DrawerListContent, NoContentWrapper } from "components/Drawer/Wrapper";
import MenuItemTitle from "components/Menu/Item/Title";
import DrawerDefaultItem from "components/Drawer/Item/Default";
// IMPORT same folder
import getGameIcon from "util/helpers/getGameIcon";
// Component: Stateless
const UserDrawerGamesList = props => (
  <div onClick={props.onClick}>
    <DrawerListContent>
      <MenuItemTitle formattedText="my-games" goTo="/my-games" largePadding />
      {props.gameListLoading ? (
        <div style={{ textAlign: "center" }}>
          <CircularProgress color="primary" size={25} />
        </div>
      ) : (
        <div>
          {props.playerGames.map(content => (
            <DrawerDefaultItem
              key={`UserDrawerGamesList ${content.name}`}
              goTo={`/my-games/${content.name}`}
              largePadding
              formattedText={content.name}
              svgIcon={getGameIcon(content.name)}
            />
          ))}
          {props.playerGames.length === 0 && (
            <Link to="/my-games">
              <NoContentWrapper>
                <Typography variant="caption">
                  <FormattedMessage id="games.you-dont-have-a-game" />
                </Typography>
                <Typography variant="caption" color="primary">
                  <FormattedMessage id="games.click-here-to-add" />
                </Typography>
              </NoContentWrapper>
            </Link>
          )}
        </div>
      )}
    </DrawerListContent>
    <Divider />
  </div>
);

const mapStateToProps = state => ({
  gameListLoading: state.app.gameListLoading,
  playerGames: state.user.games
});

export default connect(mapStateToProps)(UserDrawerGamesList);
