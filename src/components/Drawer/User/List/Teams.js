// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
// IMPORT material
import { CircularProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
// IMPORT components
import { DrawerListContent, NoContentWrapper } from "components/Drawer/Wrapper";
import MenuItemTitle from "components/Menu/Item/Title";
import DrawerTeamItem from "components/Drawer/Item/Team";
// IMPORT same folder
import getGameIcon from "util/helpers/getGameIcon";
// Component: Stateless
const UserDrawerGamesList = props => {
  const { onClick, teamListLoading, playerTeams } = props;
  return (
    <div onClick={onClick}>
      <DrawerListContent>
        <MenuItemTitle formattedText="my-teams" goTo="/my-teams" largePadding />
        {teamListLoading ? (
          <div style={{ textAlign: "center" }}>
            <CircularProgress color="primary" size={25} />
          </div>
        ) : (
          <div>
            {playerTeams &&
              playerTeams.map(content => (
                <DrawerTeamItem
                  largePadding
                  key={`UserDrawerTeamsList ${content.id}`}
                  goTo={`/team/${content.url_handle || content.id}/info`}
                  name={content.name}
                  avatar={content.thumbnail}
                  svgIcon={getGameIcon(content.game.name)}
                />
              ))}
            {props.playerTeams.length === 0 && (
              <Link to="/my-teams">
                <NoContentWrapper>
                  <Typography variant="caption">
                    <FormattedMessage id="teams.you-dont-have-a-team" />
                  </Typography>
                  <Typography variant="caption" color="primary">
                    <FormattedMessage id="teams.click-here-to-create" />
                  </Typography>
                </NoContentWrapper>
              </Link>
            )}
          </div>
        )}
      </DrawerListContent>
      <Divider />
    </div>
  );
};

const mapStateToProps = state => ({
  teamListLoading: state.app.teamListLoading,
  playerTeams: state.user.teams
});

export default connect(mapStateToProps)(UserDrawerGamesList);
