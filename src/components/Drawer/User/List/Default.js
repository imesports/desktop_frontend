// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
// IMPORT material
import Divider from "material-ui/Divider";
// IMPORT components
import DefaultDrawerItem from "components/Drawer/Item/Default";
import { DrawerListContent } from "components/Drawer/Wrapper";
// Component: Stateless
const UserDrawerDefaultList = props => (
  <div onClick={props.onClick}>
    <DrawerListContent>
      {props.list.map(content => (
        <DefaultDrawerItem
          key={`${content.text}${content.goTo}`}
          formattedText={content.text}
          icon={content.icon}
          goTo={content.goTo}
          largePadding
        />
      ))}
    </DrawerListContent>
    <Divider />
  </div>
);
// PropTypes
UserDrawerDefaultList.propTypes = {
  list: PropTypes.array.isRequired
};

export default UserDrawerDefaultList;
