import React from "react";
import styled from "styled-components";
import { withStyles } from "material-ui/styles";
import Drawer from "material-ui/Drawer";

const stylesPersistent = {
  drawerPaper: {
    top: "inherit",
    width: 280,
    overflow: "hidden",
    height: "calc(100% - 56px)"
  }
};

const stylesTemporary = {
  drawerPaper: {
    top: "inherit",
    width: 280,
    overflow: "hidden"
  }
};

const StyledPersistentDrawer = styled(Drawer)`
  && {
    position: absolute;
    z-index: 100;
    min-height: 100%;

    > {
      :hover {
        overflow-y: auto;
      }

      ::-webkit-scrollbar {
        width: 8px;
        height: 8px;
        background-color: transparent;
      }

      ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 3px transparent;
        background-color: transparent;
      }

      ::-webkit-scrollbar-thumb {
        background-color: ${props => props.theme.background.hover};
      }
    }
  }
`;

const StyledTemporaryDrawer = styled(Drawer)`
  && {
    position: absolute;
    min-height: 100%;

    > {
      :hover {
        overflow-y: auto;
      }

      ::-webkit-scrollbar {
        width: 8px;
        height: 8px;
        background-color: transparent;
      }

      ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 3px transparent;
        background-color: transparent;
      }

      ::-webkit-scrollbar-thumb {
        background-color: ${props => props.theme.background.hover};
      }
    }
  }
`;

const NoStylePersistentDrawer = props => {
  const { classes } = props;
  return (
    <StyledPersistentDrawer
      variant="persistent"
      open={props.open}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      {props.children ? props.children : ""}
    </StyledPersistentDrawer>
  );
};

const NoStyleTemporaryDrawer = props => {
  const { classes } = props;
  return (
    <StyledTemporaryDrawer
      variant="temporary"
      open={props.open}
      onClose={props.onClose}
      classes={{
        paper: classes.drawerPaper
      }}
    >
      {props.children ? props.children : ""}
    </StyledTemporaryDrawer>
  );
};

export const TemporaryDrawer = withStyles(stylesTemporary)(
  NoStyleTemporaryDrawer
);
export const PersistentDrawer = withStyles(stylesPersistent)(
  NoStylePersistentDrawer
);
