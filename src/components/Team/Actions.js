import React from "react";
import styled from "styled-components";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";

import Typography from "material-ui/Typography";
import Card from "material-ui/Card";

import CardWrapper from "components/UI/Wrapper/Card";
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";

import AccountMultiplePlusIcon from "mdi-react/AccountMultiplePlusIcon";

const StyledCardContent = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px 0;
  }
`;

const ActionItem = styled.div`
   {
    padding: ${props => props.theme.spacing.xs}px
      ${props => props.theme.spacing.default}px;
    display: flex;
    align-items: center;
    :hover {
      background-color: ${props => props.theme.background.hover};
      cursor: pointer;
    }
  }
`;

const RightSpacing = styled.div`
   {
    margin-right: ${props => props.theme.spacing.small}px;
  }
`;

const MyTeamsActions = props => {
  return (
    <CardWrapper>
      <Card>
        <StyledCardContent>
          <Link to="./my-teams/create">
            <ActionItem>
              <NoButtonIcon size="sm" fill="primary">
                <AccountMultiplePlusIcon />
              </NoButtonIcon>
              <RightSpacing />
              <Typography
                variant="body2"
                color="primary"
                style={{ textTransform: "uppercase" }}
              >
                <FormattedMessage id="create-team" />
              </Typography>
            </ActionItem>
          </Link>
        </StyledCardContent>
      </Card>
    </CardWrapper>
  );
};

export default MyTeamsActions;
