// IMPORT libs
import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
// IMPORT material
import Card from "material-ui/Card";
// IMPORT components
import CardWrapper from "components/UI/Wrapper/Card";
import CreateTeamForm from "./Form";

// Styles
import { MainContainerWrapper } from "components/UI/Wrapper/Container";

const StyledCard = styled(Card)`
   {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
// Stateless Component
const CreateTeamCard = props => {
  const { token, userId, playerGames } = props;

  return (
    <MainContainerWrapper>
      <CardWrapper>
        <StyledCard>
          {token &&
            userId && (
              <CreateTeamForm
                token={token}
                userId={userId}
                playerGames={playerGames}
              />
            )}
        </StyledCard>
      </CardWrapper>
    </MainContainerWrapper>
  );
};
// Redux
const mapStateToProps = state => ({
  token: state.user.token,
  userId: state.user.profile.user,
  playerGames: state.user.games
});

export default connect(mapStateToProps)(CreateTeamCard);
