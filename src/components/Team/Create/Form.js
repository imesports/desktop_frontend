// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT fetch
import { verifyUserHasGame } from "util/helpers";
import { createTeamFetch } from "util/team";
import { gameListFetch } from "util/game";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
// IMPORT icons
import InformationIcon from "mdi-react/InformationIcon";
// IMPORT components
import DesktopSelectInput from "components/UI/Input/DesktopSelect";
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";
import TeamProfileHeader from "components/Team/Profile/Header";
import DesktopInput from "components/UI/Input/Desktop";
import CardContent from "components/UI/Card/Content";
// STYLES
const ActionWrapper = styled.div`
   {
    display: flex;
    justify-content: flex-end;
    padding-top: ${props => props.theme.spacing.default}px;
  }
`;

const CreateFormDontHaveGameAlert = styled.div`
  && {
    display: flex;
    align-items: center;
    padding-left: 20%;
    margin-top: ${props => props.theme.spacing.small}px;
  }
`;

const CreateFormIconAlert = styled.div`
   {
    margin-right: ${props => props.theme.spacing.xxs}px;
  }
`;
// Stateful component
class CreateTeamForm extends Component {
  state = {
    avatar: null,
    avatar_preview: null,
    name: "",
    tag: "",
    game: "",
    location: "",
    language: "",
    objectives: "",
    listLoading: false,
    fetchLoading: false,
    gameList: null
  };

  componentDidMount() {
    if (this.props.token && this.props.userId && !this.state.gameList) {
      this.getGamesList(this.props.token);
    }
  }

  render() {
    const languageList = [
      {
        id: "en-US",
        name: "en"
      },
      {
        id: "pt-BR",
        name: "pt-br"
      },
      {
        id: "es-ES",
        name: "es"
      }
    ];

    const objectivesList = [
      {
        id: "FOR_FUN",
        name: "for-fun"
      },
      {
        id: "AMATEUR",
        name: "amateur"
      },
      {
        id: "PROFESSIONAL",
        name: "professional"
      }
    ];

    const {
      avatar_preview,
      name,
      tag,
      game,
      // location,
      language,
      objectives,
      listLoading,
      gameList,
      fetchLoading
    } = this.state;

    return (
      <div style={{ width: "100%" }}>
        <TeamProfileHeader
          changeAvatar={!Boolean(avatar_preview)}
          inputAvatarId="upload-team-avatar-picture"
          avatar={avatar_preview}
        />
        <input
          disabled={fetchLoading}
          style={{ display: "none" }}
          id="upload-team-avatar-picture"
          type="file"
          accept="image/*"
          onChange={e => {
            const fileList = e.target.files;
            let file = null;
            for (let i = 0; i < fileList.length; i++) {
              if (fileList[i].type.match(/^image\//)) {
                file = fileList[i];
                break;
              }
            }
            if (file !== null) {
              this.handleChange(file, "avatar");
              this.setState({
                avatar_preview: URL.createObjectURL(file)
              });
            }
          }}
        />
        <CardContent>
          <DesktopInput
            label="team-name"
            id="create_team-team_name_id"
            value={name}
            onChange={e => this.handleChange(e.target.value, "name")}
            disabled={fetchLoading}
          />
          <DesktopInput
            label="create-team.tag"
            id="create-team_tag_id"
            value={tag}
            onChange={e => this.handleChange(e.target.value, "tag")}
            disabled={fetchLoading}
            maxLength="5"
            tooltip
            tooltipTitle="create-team.tag-dialog"
            tooltipText="create-team.tag-dialog-text"
            tooltipButtonText="create-team.tag-dialog-button-text"
          />
          <DesktopSelectInput
            label="game"
            placeholder={listLoading ? "game-loading" : ""}
            id="create-team_game_id"
            value={game}
            onChange={e => this.handleChange(e.target.value, "game")}
            disabled={fetchLoading || listLoading}
            list={gameList}
          />
          {game &&
            !verifyUserHasGame(game, this.props.playerGames) && (
              <CreateFormDontHaveGameAlert>
                <CreateFormIconAlert>
                  <NoButtonIcon size="small" fill="primary">
                    <InformationIcon />
                  </NoButtonIcon>
                </CreateFormIconAlert>
                <Typography variant="body1">
                  <FormattedMessage id="create-team.dont-have-game" />
                </Typography>
              </CreateFormDontHaveGameAlert>
            )}
          {/*<DesktopInput
            label="location"
            id="create_team-location_id"
            value={location}
            onChange={e => this.handleChange(e.target.value, "location")}
            disabled={fetchLoading}
            tooltip
            tooltipTitle="create-team.location-dialog"
            tooltipText="create-team.location-dialog-text"
            tooltipButtonText="create-team.location-dialog-button-text"
          />*/}
          <DesktopSelectInput
            label="language"
            placeholder=""
            id="create-team_language_id"
            value={language}
            onChange={e => this.handleChange(e.target.value, "language")}
            disabled={fetchLoading}
            list={languageList}
          />
          <DesktopSelectInput
            label="objectives"
            placeholder=""
            id="create-team_objectives_id"
            value={objectives}
            onChange={e => this.handleChange(e.target.value, "objectives")}
            disabled={fetchLoading}
            list={objectivesList}
          />
          <ActionWrapper>
            <Button
              variant="raised"
              color="primary"
              onClick={() => this.createTeam()}
              disabled={fetchLoading}
            >
              <FormattedMessage id="done" />
            </Button>
          </ActionWrapper>
        </CardContent>
        {fetchLoading && <LinearProgress color="secondary" />}
      </div>
    );
  }

  handleChange = (value, prop) => {
    this.setState({ [prop]: value });
  };

  getGamesList = token => {
    this.setState({ listLoading: true });
    gameListFetch(token).then(res => {
      if (res.ok) {
        res.json().then(gameList => {
          this.setState({ gameList: gameList });
          this.setState({ listLoading: false });
        });
      } else {
        // Treatment of Error
      }
    });
  };

  createTeam = () => {
    this.setState({ fetchLoading: true });
    let formData = new FormData();
    const {
      avatar,
      name,
      tag,
      game,
      location,
      language,
      objectives
    } = this.state;

    const { token } = this.props;

    avatar && formData.append("avatar", avatar);
    name && formData.append("name", name);
    tag && formData.append("tag", tag);
    game && formData.append("game", game);
    location && formData.append("location", location);
    language && formData.append("language", language);
    objectives && formData.append("objectives", objectives);

    createTeamFetch(token, formData).then(res => {
      this.setState({ fetchLoading: false });
      if (res.ok) {
        res.json().then(createdTeam => {
          this.props.history.push(`/team/${createdTeam.id}/dashboard`);
        });
      } else {
        //Error
      }
    });
  };
}

CreateTeamForm.propTypes = {
  token: PropTypes.string.isRequired,
  userId: PropTypes.string.isRequired,
  playerGames: PropTypes.array.isRequired
};

export default withRouter(CreateTeamForm);
