import styled from "styled-components";

export const Container = styled.div`
   {
    display: flex;
    align-items: center;
    padding-bottom: ${props => props.theme.spacing.default}px;
  }
`;

export const TeamNameAndInfo = styled.div`
   {
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: center;
    padding-left: ${props => props.theme.spacing.default}px;
  }
`;

export const TeamGame = styled.div`
   {
    display: flex;
    align-items: center;
  }
`;

export const BottomSpacing = styled.div`
   {
    margin-bottom: ${props => props.theme.spacing.xs}px;
  }
`;

export const RightSpacing = styled.div`
   {
    margin-right: ${props => props.theme.spacing.xs}px;
  }
`;
