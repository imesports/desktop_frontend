// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
// IMPORT constants
import CONSTANTS from "appConstants";
// IMPORT utils
import GameIcon from "util/helpers/getGameIcon";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";
// IMPORT styles
import {
  Container,
  TeamNameAndInfo,
  TeamGame,
  BottomSpacing,
  RightSpacing
} from "./Wrapper";

const TeamListAvatarNameMembersGame = props => {
  const {
    avatar,
    teamName,
    teamMembers,
    teamId,
    gameName,
    // TODO: insert user position on layout
    // userId,
    // ownerId,
    memberSince
  } = props;

  return (
    <Link to={`/team/${teamId}/info`}>
      <Container>
        <CustomSizeCircleAvatar
          src={avatar || CONSTANTS.default_avatar_img}
          size="xxxl"
        />
        <TeamNameAndInfo>
          <Typography variant="headline" noWrap>
            {teamName}
          </Typography>
          <Typography
            variant="caption"
            style={{ textTransform: "lowercase" }}
            noWrap
          >
            {teamMembers}{" "}
            {teamMembers === 1 ? (
              <FormattedMessage id="member" />
            ) : (
              <FormattedMessage id="members" />
            )}
          </Typography>
          <BottomSpacing />
          <Typography variant="subheading" noWrap>
            <FormattedMessage id="member-since" /> {memberSince}
          </Typography>
          <BottomSpacing />
          <TeamGame>
            <NoButtonIcon fill="default" size="sm">
              {GameIcon(gameName)}
            </NoButtonIcon>
            <RightSpacing />
            <Typography variant="body1" noWrap>
              <FormattedMessage id={gameName} />
            </Typography>
          </TeamGame>
        </TeamNameAndInfo>
      </Container>
    </Link>
  );
};

TeamListAvatarNameMembersGame.propTypes = {
  avatar: PropTypes.string,
  teamName: PropTypes.string.isRequired,
  teamMembers: PropTypes.number.isRequired,
  teamId: PropTypes.string.isRequired,
  gameName: PropTypes.string.isRequired,
  memberSince: PropTypes.string
};

export default TeamListAvatarNameMembersGame;
