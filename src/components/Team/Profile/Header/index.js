// IMPORT libs
import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
// IMPORT material
import { CardMedia } from "material-ui/Card";
// IMPORT icons
import CameraIcon from "mdi-react/CameraIcon";
// IMPORT Global contants
import CONSTANTS from "appConstants";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
// IMPORT from parent folder
import {
  Content,
  AvatarPosition,
  FeatureImageChangeBackground,
  ClickToChangeBackground,
  AvatarChangeBackground
} from "components/UI/Wrapper/HeaderAllProfileInfo";
// Custom Styles
const StyledCardMedia = styled(CardMedia)`
   {
    height: ${props => props.theme.cardMediaSize.xl}px;
    width: 100%;
    position: relative;
    margin-bottom: calc(${props => props.theme.iconSize.xxxl}px / 2);
  }
`;
// Component: Stateless
const TeamProfileHeaderAllProfileInfo = ({
  background_image,
  avatar,
  teamName,
  changeAvatar,
  changeBackground,
  inputAvatarId,
  inputBackgroundId
}) => (
  <Content>
    <StyledCardMedia
      image={background_image || CONSTANTS.default_backgorund_img}
    >
      {changeBackground && (
        <ClickToChangeBackground
          onClick={() => document.getElementById(inputBackgroundId).click()}
        >
          <FeatureImageChangeBackground>
            <DefaultIcon fill="bodyContrast" size="medium">
              <CameraIcon />
            </DefaultIcon>
          </FeatureImageChangeBackground>
        </ClickToChangeBackground>
      )}
      <AvatarPosition>
        {changeAvatar && (
          <AvatarChangeBackground
            onClick={() => document.getElementById(inputAvatarId).click()}
          >
            <DefaultIcon fill="bodyContrast" size="medium">
              <CameraIcon />
            </DefaultIcon>
          </AvatarChangeBackground>
        )}
        <CustomSizeCircleAvatar
          src={avatar || CONSTANTS.default_avatar_img}
          size="xxxl"
          id="signup-username-avatar"
        />
      </AvatarPosition>
    </StyledCardMedia>
  </Content>
);

TeamProfileHeaderAllProfileInfo.propTypes = {
  background_image: PropTypes.string,
  avatar: PropTypes.string,
  teamName: PropTypes.string,
  changeAvatar: PropTypes.bool,
  changeBackground: PropTypes.bool,
  inputAvatarId: PropTypes.string,
  inputBackgroundId: PropTypes.string
};

export default TeamProfileHeaderAllProfileInfo;
