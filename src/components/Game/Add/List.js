// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT actions
import { updatePlayerGames } from "reducers/user/actions";
// IMPORT fetch
import { gameListFetch, addGameFetch } from "util/game";
import { verifyUserHasGame } from "util/helpers";
// IMPORT material
import { CircularProgress, LinearProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
// IMPORT components
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
// IMPORT styles
import { ListItem, ListText } from "components/Game/Wrapper";
// Stateful component
class AddGameList extends Component {
  state = {
    loadingGameList: false,
    loadingAddGame: false,
    gameList: null
  };

  componentDidMount() {
    if (this.props.token && this.props.userId && !this.state.gameList) {
      this.getGamesList(this.props.token);
    }
  }

  render() {
    const { loadingGameList, gameList, loadingAddGame } = this.state;
    const { playerGames, token, userId } = this.props;

    return (
      <div>
        {loadingAddGame && <LinearProgress color="secondary" />}
        {loadingGameList ? (
          <div style={{ textAlign: "center" }}>
            <Divider />
            <CircularProgress color="primary" />
          </div>
        ) : (
          gameList &&
          gameList
            .sort(_ => verifyUserHasGame(_.id, playerGames))
            .map((content, index) => (
              <div key={`AddGameList ${content.name}`}>
                <Divider />
                <ListItem
                  disabled={
                    verifyUserHasGame(content.id, playerGames) || loadingAddGame
                  }
                  hasGame={verifyUserHasGame(content.id, playerGames)}
                  onClick={
                    verifyUserHasGame(content.id, playerGames) || loadingAddGame
                      ? null
                      : () => this.addGame(token, userId, content.id)
                  }
                >
                  <CustomSizeCircleAvatar size="default" src={content.avatar} />
                  <ListText>
                    <Typography variant="body1" noWrap>
                      <FormattedMessage id={`${content.name}.abbreviation`} />
                    </Typography>
                    <Typography variant="caption" noWrap>
                      <FormattedMessage id={content.name} />
                    </Typography>
                  </ListText>
                </ListItem>
              </div>
            ))
        )}
      </div>
    );
  }

  getGamesList = token => {
    this.setState({ loadingGameList: true });
    gameListFetch(token).then(res => {
      if (res.ok) {
        res.json().then(gameList => {
          this.setState({ gameList: gameList });
          this.setState({ loadingGameList: false });
        });
      } else {
        // Treatment of Error
      }
    });
  };

  addGame = (token, user_id, game_id) => {
    this.setState({ loadingAddGame: true });
    addGameFetch(token, user_id, game_id).then(res => {
      if (res.ok) {
        res
          .json()
          .then(response => this.props.updatePlayerGames(response.games));
        this.setState({ loadingAddGame: false });
      } else {
        // Treatment of Error
      }
    });
  };
}

const mapDispatchToProps = dispatch => ({
  updatePlayerGames: games => dispatch(updatePlayerGames(games))
});

AddGameList.propTypes = {
  playerGames: PropTypes.array.isRequired,
  userId: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired
};

export default connect(null, mapDispatchToProps)(AddGameList);
