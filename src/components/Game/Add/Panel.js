// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
import Card from "material-ui/Card";
// IMPORT components
import GameList from "./List";
// IMPORT styles
import { PanelTitle } from "components/Game/Wrapper";
// Stateless component
const AddGamePanel = props => {
  return (
    <Card>
      <PanelTitle>
        <Typography color="primary" variant="subheading">
          <FormattedMessage id="add-game" />
        </Typography>
      </PanelTitle>
      <GameList
        token={props.token}
        userId={props.userId}
        playerGames={props.playerGames}
      />
    </Card>
  );
};

AddGamePanel.propTypes = {
  playerGames: PropTypes.array.isRequired,
  userId: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired
};

export default AddGamePanel;
