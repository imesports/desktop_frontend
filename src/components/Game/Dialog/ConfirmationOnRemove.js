//IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Button from "material-ui/Button";
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from "material-ui/Dialog";

const DialogConfirmationOnRemove = props => {
  const { open, closeAction, confirmAction, gameAbbreviation } = props;
  return (
    <Dialog open={open} onClose={closeAction}>
      <DialogTitle>
        <FormattedMessage id="remove-game-confirmation.title" />{" "}
        <FormattedMessage id={gameAbbreviation} />?
      </DialogTitle>
      <DialogContent>
        <DialogContentText>
          <FormattedMessage id="remove-game-confirmation.text" />
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={closeAction} color="primary">
          <FormattedMessage id="cancel" />
        </Button>
        <Button onClick={confirmAction} color="primary" autoFocus>
          <FormattedMessage id="remove-game" />
        </Button>
      </DialogActions>
    </Dialog>
  );
};

DialogConfirmationOnRemove.propTypes = {
  open: PropTypes.bool.isRequired,
  closeAction: PropTypes.func.isRequired,
  confirmAction: PropTypes.func.isRequired,
  gameAbbreviation: PropTypes.string.isRequired
};

export default DialogConfirmationOnRemove;
