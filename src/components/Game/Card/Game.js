// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { withRouter } from "react-router";
import PropTypes from "prop-types";
// IMPORT material
import { LinearProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
import Button from "material-ui/Button";
import Card from "material-ui/Card";
// IMPORT components
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
import MenuGameOptions from "components/Game/Menu/GameOptions";
import CardContent from "components/UI/Card/Content";
import CardWrapper from "components/UI/Wrapper/Card";
// IMPORT Icons
import DotsVerticalIcon from "mdi-react/DotsVerticalIcon";
// IMPORT styles
import {
  ActionsWrapper,
  GameTitleAndDescription,
  ImageAndTitleWrapper,
  GameCardMedia,
  StatsWrapper,
  CardOptions,
  StatsTitle,
  Spacing
} from "components/Game/Wrapper";
// Stateless component
class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gameOptions: null,
      loading: false
    };
  }

  render() {
    const {
      gameId,
      lastUpdatedTime,
      backgroundImage,
      statsName,
      stats,
      name,
      goTo,
      history,
      statsDiff
    } = this.props;

    const { gameOptions, loading } = this.state;

    return (
      <CardWrapper>
        {loading && <LinearProgress color="secondary" />}
        <Card>
          <ImageAndTitleWrapper>
            <GameCardMedia image={backgroundImage} />
            <GameTitleAndDescription>
              <CardContent>
                <Typography variant="display2" noWrap>
                  <FormattedMessage id={`${name}.abbreviation`} />
                </Typography>
                <Typography variant="body2" noWrap>
                  <FormattedMessage id={name} />
                </Typography>
                <Spacing />
                <Typography variant="body1">
                  <FormattedMessage id={`${name}.description`} />
                </Typography>
              </CardContent>
            </GameTitleAndDescription>
            <CardOptions>
              <DefaultIcon
                fill="default"
                size="sm"
                onClick={this.openMenu("gameOptions")}
              >
                <DotsVerticalIcon />
              </DefaultIcon>
            </CardOptions>
          </ImageAndTitleWrapper>
          <Divider />
          <ActionsWrapper>
            <Typography>
              <FormattedMessage id="last-updated" />: <b>{lastUpdatedTime}</b>
            </Typography>
            <StatsWrapper>
              <StatsTitle>
                <Typography variant="button">
                  {statsName && <FormattedMessage id={statsName} />}
                </Typography>
              </StatsTitle>
              <Typography variant="button">
                {stats && statsDiff && `${stats} (${statsDiff})`}
              </Typography>
            </StatsWrapper>
            <Button color="primary" onClick={() => history.push(goTo)}>
              <FormattedMessage id="view-more" />
            </Button>
          </ActionsWrapper>
        </Card>
        <MenuGameOptions
          gameId={gameId}
          gameAbbreviation={`${name}.abbreviation`}
          anchorEl={gameOptions}
          closeAction={() => this.closeMenu("gameOptions")}
          loadingToggle={this.loadingToggle}
        />
      </CardWrapper>
    );
  }

  openMenu = menu => event => {
    this.setState({
      [menu]: event.currentTarget
    });
  };

  closeMenu = menu => {
    this.setState({
      [menu]: null
    });
  };

  loadingToggle = value => {
    this.setState({
      loading: value
    });
  };
}

Game.propTypes = {
  gameId: PropTypes.number.isRequired,
  lastUpdatedTime: PropTypes.string.isRequired,
  backgroundImage: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  goTo: PropTypes.string.isRequired,
  statsName: PropTypes.string,
  statsDiff: PropTypes.string,
  stats: PropTypes.string
};

export default withRouter(Game);
