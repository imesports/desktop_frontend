import styled from "styled-components";
import { CardMedia } from "material-ui/Card";

export const ImageAndTitleWrapper = styled.div`
   {
    position: relative;
    display: flex;
    justify-content: flex-start;
  }
`;

export const ActionsWrapper = styled.div`
   {
    display: flex;
    justify-content: space-between;
    align-items: center;

    padding: 16px;
    width: 100%;
  }
`;

export const StatsWrapper = styled.div`
   {
    display: flex;
    justify-content: space-around;
  }
`;

export const StatsTitle = styled.div`
   {
    margin-right: 16px;
  }
`;

export const GameCardMedia = styled(CardMedia)`
   {
    width: 400px;
    height: 200px;
  }
`;

export const GameTitleAndDescription = styled.div`
   {
    width: 280px;
  }
`;

export const Spacing = styled.div`
   {
    margin-bottom: 16px;
  }
`;

export const ListItem = styled.div`
   {
    display: flex;
    align-items: center;
    width: 100%;
    padding: ${props => props.theme.spacing.default}px;

    cursor: ${props => (props.disabled ? "default" : "pointer")};
    :hover {
      ${props =>
        !props.disabled && `background-color: ${props.theme.background.hover}`};
      transition: ${props => (props.disabled ? 0 : 0.2)}s;
    }

    ${props => props.hasGame && "filter: grayscale(1)"};
  }
`;

export const ListText = styled.div`
   {
    width: calc(100% - ${props => props.theme.iconSize.default}px);
    padding-left: ${props => props.theme.spacing.default}px;
  }
`;

export const PanelTitle = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
  }
`;

export const CardOptions = styled.div`
   {
    position: absolute;
    right: ${props => props.theme.spacing.xs}px;
    top: ${props => props.theme.spacing.xs}px;
  }
`;
