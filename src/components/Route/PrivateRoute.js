import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { Route } from "react-router";
import { connect } from "react-redux";

import { LoggedInWrapper } from "components/UI/Wrapper/Container";

import { getProfile } from "reducers/user/actions";
import { getPlayerGames } from "reducers/user/actions";
import { getPlayerTeams } from "reducers/user/actions";

class PrivateRoute extends Component {
  componentDidMount() {
    const {
      userProfile,
      token,
      getProfile,
      getPlayerGames,
      getPlayerTeams
    } = this.props;
    if (
      Object.keys(userProfile).length === 0 &&
      userProfile.constructor === Object &&
      token
    ) {
      getProfile({ token });
      getPlayerGames(token);
      getPlayerTeams(token);
    }
  }

  render() {
    const {
      noLeftMargin = false,
      drawerShow,
      token,
      component: Component,
      ...rest
    } = this.props;
    return (
      <LoggedInWrapper drawer={drawerShow} noLeftMargin={noLeftMargin}>
        <Route
          {...rest}
          render={props =>
            token ? (
              <Component {...props} />
            ) : (
              <Redirect
                to={{
                  pathname: "/",
                  state: { from: props.location }
                }}
              />
            )
          }
        />
      </LoggedInWrapper>
    );
  }
}
// Redux Store / State
const mapStateToProps = state => ({
  drawerShow: state.app.drawerShow,
  userProfile: state.user.profile,
  token: state.user.token
});
// Redux dispatch
const mapDispatchToProps = dispatch => ({
  getProfile: token => dispatch(getProfile(token)),
  getPlayerGames: token => dispatch(getPlayerGames(token)),
  getPlayerTeams: token => dispatch(getPlayerTeams(token))
});

export default connect(mapStateToProps, mapDispatchToProps)(PrivateRoute);
