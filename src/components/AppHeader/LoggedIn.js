// IMPORT libs
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ReactSVG from "react-svg";
// IMPORT material
import Hidden from "material-ui/Hidden";
// IMPORT icons
import AccountCircleIcon from "mdi-react/AccountCircleIcon";
import MenuIcon from "mdi-react/MenuIcon";
// import BellIcon from "mdi-react/BellIcon";
import AppsIcon from "mdi-react/AppsIcon";
// IMPORT image
import Logo from "imgs/logo.svg";
// IMPORT components
import { ThemedAppBar, ThemedToolbar } from "components/UI/AppBar/LoggedIn";
import CustomSizeCircleAvatar from "components/UI/Avatar/CustomSizeCircle";
import { LoggedLogoWrapper } from "components/UI/Wrapper/Logo";
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
// import NotificationsMenu from "components/Menu/Notifications";
import MenuAndLogo from "components/UI/Wrapper/MenuAndLogo";
import SearchInput from "components/UI/Input/Search";
import AccountMenu from "components/Menu/Account";
import LogoImg from "components/UI/Img/Logo";
import AppsMenu from "components/Menu/Apps";
// IMPORT from same folder
import { SearchAndActions, Actions } from "./Wrapper";
// Component: Stateful
class AppHeaderLoggedIn extends Component {
  state = {
    appsMenu: null,
    notificationsMenu: null,
    userInfoAndOptions: null
  };

  render() {
    const {
      toggleMenu,
      persistentDrawer,
      userProfile,
      appComponent
    } = this.props;
    const {
      appsMenu,
      // notificationsMenu,
      userInfoAndOptions
    } = this.state;
    return (
      <ThemedAppBar position="fixed">
        <ThemedToolbar>
          <MenuAndLogo>
            <Hidden mdDown>
              <DefaultIcon
                fill="default"
                onClick={() =>
                  toggleMenu(
                    "persistentDrawer",
                    persistentDrawer ? false : true
                  )
                }
              >
                <MenuIcon />
              </DefaultIcon>
            </Hidden>
            <Hidden lgUp>
              <DefaultIcon
                fill="default"
                onClick={() => toggleMenu("temporaryDrawer", true)}
              >
                <MenuIcon />
              </DefaultIcon>
            </Hidden>
            <LoggedLogoWrapper>
              <Link to="/home">
                <LogoImg color="primary" size="loggedIn">
                  <ReactSVG path={Logo} />
                </LogoImg>
              </Link>
            </LoggedLogoWrapper>
          </MenuAndLogo>
          <SearchAndActions>
            <SearchInput />
            <Actions>
              <DefaultIcon
                fill="default"
                aria-haspopup="true"
                onClick={event => this.openMenu("appsMenu", event)}
              >
                <AppsIcon />
              </DefaultIcon>
              {/*<DefaultIcon
                fill="default"
                aria-haspopup="true"
                onClick={event => this.openMenu("notificationsMenu", event)}
              >
                <BellIcon />
              </DefaultIcon>*/}
              <DefaultIcon
                fill="default"
                aria-haspopup="true"
                onClick={event => this.openMenu("userInfoAndOptions", event)}
              >
                {userProfile.thumbnail ? (
                  <CustomSizeCircleAvatar
                    size="medium"
                    src={userProfile.thumbnail}
                  />
                ) : (
                  <AccountCircleIcon />
                )}
              </DefaultIcon>
              <AppsMenu appsMenu={appsMenu} closeMenu={this.closeMenu} />
              {/*<NotificationsMenu
                notificationsMenu={notificationsMenu}
                closeMenu={this.closeMenu}
              />*/}
              <AccountMenu
                userInfoAndOptions={userInfoAndOptions}
                closeMenu={this.closeMenu}
                appComponent={appComponent}
              />
            </Actions>
          </SearchAndActions>
        </ThemedToolbar>
      </ThemedAppBar>
    );
  }

  openMenu = (menu, event) => {
    this.setState({
      [menu]: event.currentTarget
    });
  };

  closeMenu = menu => {
    this.setState({
      [menu]: null
    });
  };
}
// PropTypes
AppHeaderLoggedIn.propTypes = {
  appComponent: PropTypes.object.isRequired,
  userProfile: PropTypes.object.isRequired,
  toggleMenu: PropTypes.func.isRequired,
  persistentDrawer: PropTypes.bool.isRequired
};
// Redux Store / State
const mapStateToProps = state => ({
  userProfile: state.user.profile
});

export default connect(mapStateToProps)(AppHeaderLoggedIn);
