// IMPORT libs
import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT Redux Actions
import { drawerShow } from "reducers/app/actions";
// IMPORT components
import UserDrawer from "components/Drawer/User";
// IMPORT from same folder
import AppHeaderLoggedIn from "./LoggedIn";
// Component: Stateful
class HeaderWithDrawer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      persistentDrawer: true,
      temporaryDrawer: false
    };
  }

  render() {
    return (
      <div>
        <AppHeaderLoggedIn
          toggleMenu={this.toggleMenu}
          persistentDrawer={this.state.persistentDrawer}
          temporaryDrawer={this.state.temporaryDrawer}
          appComponent={this.props.appComponent}
        />
        <UserDrawer
          persistentDrawer={this.state.persistentDrawer}
          temporaryDrawer={this.state.temporaryDrawer}
          toggleMenu={this.toggleMenu}
        />
      </div>
    );
  }

  toggleMenu = (menu, state) => {
    this.setState({
      [menu]: state
    });
    if (menu === "persistentDrawer") {
      this.props.drawerShow(state);
    }
  };
}
// PropTypes
HeaderWithDrawer.propTypes = {
  appComponent: PropTypes.object.isRequired
};
// Use dispatch
export default connect(null, { drawerShow })(HeaderWithDrawer);
