// IMPORT libs
import React from "react";
import { connect } from "react-redux";
// IMPORT from same folder
import HeaderWithDrawer from "./HeaderWithDrawer";
import PublicHeader from "./Public";
// Component: Stateless
const AppHeader = props => {
  const { appComponent } = props;
  const { pathname } = props.router.location;

  if (pathname === "/" || pathname === "/signup/last-step") {
    return <PublicHeader appComponent={appComponent} pathname={pathname} />;
  }

  if (pathname === "/home") {
    return <HeaderWithDrawer appComponent={appComponent} />;
  }

  return null;
};

const mapStateToProps = state => ({
  router: state.router
});

export default connect(mapStateToProps)(AppHeader);
