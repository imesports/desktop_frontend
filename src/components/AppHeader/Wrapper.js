import styled from "styled-components";

export const SearchAndActions = styled.div`
   {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
  }
`;

export const Actions = styled.div`
   {
    display: flex;
    align-items: center;
  }
`;
