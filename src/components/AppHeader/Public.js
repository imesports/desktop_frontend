// IMPORT libs
import React from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import ReactSVG from "react-svg";
import { compose } from "redux";
// IMPORT icons
import LightbulbOutlineIcon from "mdi-react/LightbulbOutlineIcon";
import LightbulbOnIcon from "mdi-react/LightbulbOnIcon";
// IMPORT Redux Actions
import { swapTheme } from "reducers/app/actions";
// IMPORT components
import {
  ThemedAppBar,
  ThemedToolbar,
  IconDiv
} from "components/UI/AppBar/Public";
import { PublicLogoWrapper } from "components/UI/Wrapper/Logo";
import { ThemedIconButton } from "components/UI/Icon";
import LogoImg from "components/UI/Img/Logo";
// IMPORT image
import Logo from "imgs/logo.svg";
// Component: Stateless
const AppHeaderPublic = props => (
  <ThemedAppBar position="static">
    <ThemedToolbar>
      <IconDiv />

      <PublicLogoWrapper>
        <LogoImg color="primary" size="public">
          <ReactSVG path={Logo} />
        </LogoImg>
      </PublicLogoWrapper>

      <IconDiv
        onClick={() => {
          props.swapTheme();
          props.appComponent.forceUpdate();
        }}
      >
        <ThemedIconButton>
          {props.theme_color === "light" ? (
            <LightbulbOnIcon />
          ) : (
            <LightbulbOutlineIcon />
          )}
        </ThemedIconButton>
      </IconDiv>
    </ThemedToolbar>
  </ThemedAppBar>
);
// Redux Store / State
const mapStateToProps = state => ({
  theme_color: state.app.theme
});
// Use dispatch
export default compose(connect(mapStateToProps, { swapTheme }), withRouter)(
  AppHeaderPublic
);
