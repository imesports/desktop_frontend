import styled from "styled-components";
import Popover from "material-ui/Popover";

export const StyledPopover = styled(Popover)`
  && {
    margin-top: ${props => props.theme.spacing.xxs}px;
  }
`;

export const PopoverWrapper = styled.div`
   {
    width: ${props => props.theme.menuSize[props.size]}px;
    min-width: ${props => props.theme.menuSize[props.size]}px;
  }
`;

export const PopoverContent = styled.div`
   {
    padding: ${props => props.theme.spacing.xs}px 0;
  }
`;

export const HeaderWrapper = styled.div`
   {
    padding: ${props => props.theme.spacing.xs}px 0;
    background-color: ${props => props.theme.background.menuHeader};
    width: 100%;
  }
`;

export const PopoverWithText = styled.div`
   {
    padding: ${props => props.theme.spacing.default}px;
  }
`;

export const PopoverActions = styled.div`
   {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 0 ${props => props.theme.spacing.default}px
      ${props => props.theme.spacing.default}px;
  }
`;
