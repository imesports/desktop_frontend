// IMPORT libs
import React from "react";
import { injectIntl } from "react-intl";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { compose } from "redux";
// IMPORT icons
import LightbulbOnOutlineIcon from "mdi-react/LightbulbOnOutlineIcon";
// import MessageAlertIcon from "mdi-react/MessageAlertIcon";
// import HelpCircleIcon from "mdi-react/HelpCircleIcon";
import ExitToAppIcon from "mdi-react/ExitToAppIcon";
// import SettingsIcon from "mdi-react/SettingsIcon";
// Redux actions
import { logoutUser } from "reducers/user/actions";
// IMPORT material
import Divider from "material-ui/Divider";
// IMPORT components
import CardHeaderUserAvatarNameEmail from "components/UI/Card/Header/UserAvatarNameEmail";
// IMPORT from parent folder
import { PopoverContent, HeaderWrapper } from "../Wrapper";
import DefaultMenuItem from "../Item/Default";
// Component: Stateless
const MenuAccountUserInfoAndOptions = props => {
  const {
    // closeMenu,
    logoutUser,
    intl,
    theme,
    changeMenu
  } = props;
  // const closeUserInfoAndOptionsMenu = () => closeMenu("userInfoAndOptions");
  return (
    <div>
      <HeaderWrapper>
        <CardHeaderUserAvatarNameEmail />
      </HeaderWrapper>
      <PopoverContent>
        <DefaultMenuItem
          formattedText="logout"
          icon={ExitToAppIcon}
          onClick={logoutUser}
        />
      </PopoverContent>
      <Divider />
      <PopoverContent>
        <DefaultMenuItem
          text={`${intl.formatMessage({
            id: "dark-theme"
          })}: ${
            theme === "dark"
              ? intl.formatMessage({ id: "menu.dark-theme-on" })
              : intl.formatMessage({ id: "menu.dark-theme-off" })
          }`}
          icon={LightbulbOnOutlineIcon}
          onClick={() => changeMenu("darkThemeMenu")}
          chevronRight
        />
        {/*<DefaultMenuItem
          formattedText="settings"
          icon={SettingsIcon}
          goTo="/settings"
          onClick={() => closeUserInfoAndOptionsMenu()}
        />
        <DefaultMenuItem
          formattedText="help"
          icon={HelpCircleIcon}
          goTo="/help"
          onClick={() => closeUserInfoAndOptionsMenu()}
        />
        <DefaultMenuItem
          formattedText="send-feedback"
          icon={MessageAlertIcon}
          goTo="/feedback"
          onClick={() => closeUserInfoAndOptionsMenu()}
        />*/}
      </PopoverContent>
    </div>
  );
};
// PropTypes
MenuAccountUserInfoAndOptions.propTypes = {
  changeMenu: PropTypes.func.isRequired
};
// Redux Store / State
const mapStateToProps = state => ({
  theme: state.app.theme
});
// Use dispatch
export default compose(connect(mapStateToProps, { logoutUser }), injectIntl)(
  MenuAccountUserInfoAndOptions
);
