// IMPORT libs
import React from "react";
import { FormattedMessage, FormattedHTMLMessage } from "react-intl";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
import Switch from "material-ui/Switch";
// IMPORT Redux Actions
import { swapTheme } from "reducers/app/actions";
// IMPORT from parent folder
import { HeaderWrapper, PopoverWithText, PopoverActions } from "../Wrapper";
import HeaderTitleWithBackButton from "../Header/TitleWithBackButton";
// Component: Stateless
const DarkThemeMenu = props => (
  <div>
    <HeaderWrapper>
      <HeaderTitleWithBackButton
        title="dark-theme"
        iconClick={() => props.iconClick("userInfoAndOptions")}
      />
    </HeaderWrapper>
    <PopoverWithText>
      <Typography>
        <FormattedHTMLMessage id="menu.dark-theme" />
      </Typography>
    </PopoverWithText>
    <PopoverActions>
      <Typography variant="caption" style={{ textTransform: "uppercase" }}>
        <FormattedMessage id="dark-theme" />
      </Typography>
      <Switch
        color="primary"
        value="dark-theme"
        checked={props.theme === "dark"}
        onChange={() => {
          props.swapTheme();
          props.appComponent.forceUpdate();
        }}
      />
    </PopoverActions>
  </div>
);
// PropTypes
DarkThemeMenu.propTypes = {
  iconClick: PropTypes.func.isRequired
};
// Redux Store / State
const mapStateToProps = state => ({
  theme: state.app.theme
});
// Use dispatch
export default connect(mapStateToProps, { swapTheme })(DarkThemeMenu);
