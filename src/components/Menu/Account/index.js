// IMPORT libs
import React, { Component } from "react";
import PropTypes from "prop-types";
// IMPORT material
import Popover from "material-ui/Popover";
// IMPORT from folder
import MenuItemUserInfoAndOptions from "./UserInfoAndOptions";
import MenuItemDarkTheme from "./DarkTheme";
// IMPORT from parent folder
import { PopoverWrapper } from "../Wrapper";
// Component: Stateful
class MenuAccount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedMenu: "userInfoAndOptions"
    };
  }

  render() {
    const { userInfoAndOptions, closeMenu, appComponent } = this.props;
    return (
      <Popover
        id="menu-account-id"
        anchorEl={userInfoAndOptions}
        open={Boolean(userInfoAndOptions)}
        onClose={() => closeMenu("userInfoAndOptions")}
        onExited={() => this.changeMenu("userInfoAndOptions")}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right"
        }}
        margin="small"
      >
        <PopoverWrapper size="pictureMenu">
          {
            {
              userInfoAndOptions: (
                <MenuItemUserInfoAndOptions
                  closeMenu={closeMenu}
                  changeMenu={this.changeMenu}
                />
              ),
              darkThemeMenu: (
                <MenuItemDarkTheme
                  appComponent={appComponent}
                  iconClick={this.changeMenu}
                />
              )
            }[this.state.selectedMenu]
          }
        </PopoverWrapper>
      </Popover>
    );
  }

  changeMenu = menu => {
    this.setState({ selectedMenu: menu });
  };
}
// PropTypes
MenuAccount.propTypes = {
  userInfoAndOptions: PropTypes.object,
  appComponent: PropTypes.object.isRequired
};

export default MenuAccount;
