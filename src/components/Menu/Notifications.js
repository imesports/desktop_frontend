// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import BellIcon from "mdi-react/BellIcon";
// IMPORT componentds
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";
// IMPORT from same folder
import { StyledPopover, PopoverWrapper, HeaderWrapper } from "./Wrapper";
import HeaderTitleSettingsIcon from "./Header/TitleSettingsIcon";
// Custom Styles
const Spacing = styled.div`
   {
    margin-bottom: ${props => props.theme.spacing.small}px;
  }
`;

export const NoNotificationsContent = styled.div`
   {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: ${props => props.theme.menuSize.notificationsMenuHeight}px;
    padding: ${props => props.theme.spacing.large}px;
    width: 100%;
  }
`;
// Component: Stateless
const NotificationsMenu = props => (
  <StyledPopover
    id="notifications-menu-id"
    anchorEl={props.notificationsMenu}
    open={Boolean(props.notificationsMenu)}
    onClose={() => props.closeMenu("notificationsMenu")}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "right"
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "right"
    }}
    margin="xxs"
  >
    <PopoverWrapper size="notificationsMenu">
      <HeaderWrapper>
        <HeaderTitleSettingsIcon title="notifications" iconClick={() => null} />
      </HeaderWrapper>
      <NoNotificationsContent>
        <NoButtonIcon fill="lightDefault" size="xxxl">
          <BellIcon />
        </NoButtonIcon>
        <Spacing />
        <Typography align="center">
          <FormattedMessage id="notitications.no-notification-title" />
        </Typography>
        <Spacing />
        <Typography variant="body2" align="center">
          <FormattedMessage id="notitications.no-notification-subtitle" />
        </Typography>
      </NoNotificationsContent>
    </PopoverWrapper>
  </StyledPopover>
);
// PropTypes
NotificationsMenu.propTypes = {
  notificationsMenu: PropTypes.object,
  closeMenu: PropTypes.func.isRequired
};

export default NotificationsMenu;
