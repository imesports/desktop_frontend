// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import ChevronRightIcon from "mdi-react/ChevronRightIcon";
// IMPORT components
import { NoButtonIcon } from "components/UI/Icon/IconFormatter";
// Custom Styles
const MenuItemWrapper = styled.div`
   {
    display: flex;
    width: 100%;
    align-items: center;

    padding: ${props => props.theme.spacing.xs}px
      ${props => props.theme.spacing.default}px;

    :hover {
      cursor: pointer;
      background-color: ${props => props.theme.background.hover};
    }
  }
`;

const TextWrapper = styled.div`
   {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-left: ${props => props.theme.spacing.large}px;
  }
`;
// Component: Stateless
const DefaultMenuItem = props => {
  const {
    goTo,
    largePadding = false,
    formattedText,
    text,
    onClick,
    chevronRight
  } = props;
  return (
    <div>
      {goTo ? (
        <Link to={goTo}>
          <MenuItemWrapper onClick={onClick} largePadding={largePadding}>
            <NoButtonIcon size="sm" fill="default">
              <props.icon />
            </NoButtonIcon>
            <TextWrapper>
              <Typography variant="body1">
                {formattedText && <FormattedMessage id={formattedText} />}
                {text}
              </Typography>
              {chevronRight && (
                <NoButtonIcon size="sm" fill="default">
                  <ChevronRightIcon />
                </NoButtonIcon>
              )}
            </TextWrapper>
          </MenuItemWrapper>
        </Link>
      ) : (
        <MenuItemWrapper onClick={onClick}>
          <NoButtonIcon size="sm" fill="default">
            <props.icon />
          </NoButtonIcon>
          <TextWrapper>
            <Typography variant="body1">
              {formattedText && <FormattedMessage id={formattedText} />}
              {text}
            </Typography>
            {chevronRight && (
              <NoButtonIcon size="sm" fill="default">
                <ChevronRightIcon />
              </NoButtonIcon>
            )}
          </TextWrapper>
        </MenuItemWrapper>
      )}
    </div>
  );
};
// PropTypes
DefaultMenuItem.propTypes = {
  onClick: PropTypes.func.isRequired,
  formattedText: PropTypes.string,
  icon: PropTypes.func.isRequired,
  text: PropTypes.string,
  goTo: PropTypes.string
};

export default DefaultMenuItem;
