// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import styled from "styled-components";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// Custom Styles
const MenuItemWrapper = styled.div`
   {
    display: flex;
    width: 100%;
    align-items: center;

    text-transform: uppercase;

    padding: ${props => props.theme.spacing.xs}px
      ${props => props.theme.spacing.largeDefault}px;

    :hover {
      cursor: pointer;
    }
  }
`;

const StyledTypography = styled(Typography)`
  && {
    color: ${props => props.theme.icon.default};
  }
`;
// Component: Stateless
const MenuItemTitle = ({ goTo, largePadding = false, formattedText, text }) => (
  <Link to={goTo}>
    <MenuItemWrapper largePadding={largePadding}>
      <StyledTypography variant="body2">
        {formattedText && <FormattedMessage id={formattedText} />}
        {text}
      </StyledTypography>
    </MenuItemWrapper>
  </Link>
);
// PropTypes
MenuItemTitle.propTypes = {
  formattedText: PropTypes.string,
  text: PropTypes.string,
  goTo: PropTypes.string.isRequired,
  largePadding: PropTypes.bool
};

export default MenuItemTitle;
