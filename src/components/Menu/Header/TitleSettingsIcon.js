import React from "react";
import styled from "styled-components";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import SettingsIcon from "mdi-react/SettingsIcon";
// IMPORT components
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
// Custom Styles
const TitleWrapper = styled.div`
   {
    display: flex;
    width: 100%;
    align-items: center;
    justify-content: space-between;

    padding: 0 ${props => props.theme.spacing.small}px;
  }
`;
// Component: Stateless
const MenuHeaderTitleSettingsIcon = props => (
  <TitleWrapper>
    <Typography variant="subheading">
      <FormattedMessage id={props.title} />
    </Typography>
    <DefaultIcon size="sm" fill="default" onClick={props.iconClick}>
      <SettingsIcon />
    </DefaultIcon>
  </TitleWrapper>
);
// PropTypes
MenuHeaderTitleSettingsIcon.propTypes = {
  iconClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default MenuHeaderTitleSettingsIcon;
