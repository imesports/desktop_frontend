// IMPORT libs
import React from "react";
import styled from "styled-components";
import { FormattedMessage } from "react-intl";
import PropTypes from "prop-types";
// IMPORT material
import Typography from "material-ui/Typography";
// IMPORT icons
import ArrowLeftIcon from "mdi-react/ArrowLeftIcon";
// IMPORT components
import { DefaultIcon } from "components/UI/Icon/IconFormatter";
// Custom Styles
const TitleWrapper = styled.div`
   {
    display: flex;
    width: 100%;
    align-items: center;
  }
`;
// Component: Stateless
const HeaderTitleWithBackButton = props => (
  <TitleWrapper>
    <DefaultIcon size="sm" fill="bodyContrast" onClick={props.iconClick}>
      <ArrowLeftIcon />
    </DefaultIcon>
    <Typography variant="subheading">
      <FormattedMessage id={props.title} />
    </Typography>
  </TitleWrapper>
);
// PropTypes
HeaderTitleWithBackButton.propTypes = {
  iconClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
};

export default HeaderTitleWithBackButton;
