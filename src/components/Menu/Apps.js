// IMPORT libs
import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
// IMPORT material
import Popover from "material-ui/Popover";
// IMPORT icons
import AccountMultipleIcon from "mdi-react/AccountMultipleIcon";
import GamepadVariantIcon from "mdi-react/GamepadVariantIcon";
// import TrophyIcon from "mdi-react/TrophyIcon";
// IMPORT from same folder
import { PopoverWrapper, PopoverContent } from "./Wrapper";
import DefaultMenuItem from "./Item/Default";
// Component: Stateless
const AppsMenu = props => {
  const appsList = [
    { title: "teams", icon: AccountMultipleIcon, goTo: "/my-teams" },
    { title: "games", icon: GamepadVariantIcon, goTo: "/my-games" }
    // { title: "championships", icon: TrophyIcon, goTo: "/championships" }
  ];
  const { appsMenu, closeMenu } = props;
  const closeAppsMenu = () => closeMenu("appsMenu");
  return (
    <Popover
      id="apps-menu-id"
      anchorEl={appsMenu}
      open={Boolean(appsMenu)}
      onClose={() => closeAppsMenu()}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right"
      }}
      transformOrigin={{
        vertical: "top",
        horizontal: "right"
      }}
      margin="small"
    >
      <PopoverWrapper size="appsMenu">
        <PopoverContent>
          {appsList.map(item => (
            <DefaultMenuItem
              key={`DefaultMenuItem${item.title}`}
              formattedText={item.title}
              icon={item.icon}
              goTo={item.goTo}
              onClick={() => closeAppsMenu()}
            />
          ))}
        </PopoverContent>
      </PopoverWrapper>
    </Popover>
  );
};
// PropTypes
AppsMenu.propTypes = {
  appsMenu: PropTypes.object,
  closeMenu: PropTypes.func.isRequired
};

export default withRouter(AppsMenu);
