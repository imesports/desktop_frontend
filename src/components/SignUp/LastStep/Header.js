// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// IMPORT Redux Actions
import { logoutUser } from "reducers/user/actions";
// IMPORT material
import Typography from "material-ui/Typography";
import Divider from "material-ui/Divider";
// IMPORT icons
import CloseIcon from "mdi-react/CloseIcon";
// IMPORT components
import { IconDiv } from "components/UI/AppBar/Public";
import { ThemedIconButton } from "components/UI/Icon";
// Custom Styles
const Container = styled.div`
   {
    display: flex;
    flex-direction: column;
  }
`;

const Centralize = styled.div`
   {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;

const StyledDivider = styled(Divider)`
  && {
    margin: ${props => props.theme.spacing.default}px 0;
    width: 100%;
  }
`;
// Component: Stateless
const SignUpLastStepHeader = ({ logout, title, subtitle }) => (
  <Container>
    <Centralize>
      <IconDiv>
        <ThemedIconButton onClick={logout}>
          <CloseIcon />
        </ThemedIconButton>
      </IconDiv>
      <Typography variant="display1">
        <FormattedMessage id={title} />
      </Typography>
      <IconDiv />
    </Centralize>
    <StyledDivider />
    <Typography variant="subheading">
      <FormattedMessage id={subtitle} />
    </Typography>
  </Container>
);
// PropTypes
SignUpLastStepHeader.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string.isRequired
};
// Redux dispatch
const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutUser())
});

export default connect(null, mapDispatchToProps)(SignUpLastStepHeader);
