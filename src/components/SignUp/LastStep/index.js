// IMPORT libs
import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import styled from "styled-components";
import { connect } from "react-redux";
// IMPORT material
import Button from "material-ui/Button";
// IMPORT Redux Actions
import { savePlayerProfile } from "reducers/user/actions";
// IMPORT components
import PlayerProfileHeader from "components/Player/Profile/Header";
import LoadingFullScreen from "components/UI/Loading/FullScreen";
import DesktopInput from "components/UI/Input/Desktop";
import CardContent from "components/UI/Card/Content";
// Custom Styles
const Content = styled.div`
   {
    width: 100%;
  }
`;

const ButtonContent = styled.div`
   {
    margin-top: ${props => props.theme.spacing.default}px;
    width: 100%;
    display: flex;
    justify-content: flex-end;
  }
`;
// Component: Stateful
class SignUpLastStep extends Component {
  constructor(props) {
    super(props);

    this.state = {
      changeBackground: true,
      changeAvatar: true,
      background_image_preview: props.userProfile.background_image || null,
      avatar_preview: props.userProfile.avatar || null,
      background_image: null,
      avatar: null,
      first_name: props.userProfile.first_name || "",
      display_name: props.userProfile.display_name || "",
      last_name: props.userProfile.last_name || "",
      headline: props.userProfile.headline || "",
      location: props.userProfile.location || ""
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      !nextProps.isLoading &&
      this.props.userProfile !== nextProps.userProfile
    ) {
      this.setState({
        avatar_preview: nextProps.userProfile.avatar || null,
        background_image_preview:
          nextProps.userProfile.background_image || null,
        first_name: nextProps.userProfile.first_name || "",
        display_name: nextProps.userProfile.display_name || "",
        last_name: nextProps.userProfile.last_name || "",
        headline: nextProps.userProfile.headline || "",
        location: nextProps.userProfile.location || ""
      });
    }
  }

  render() {
    return (
      <Content>
        {this.props.isLoading && <LoadingFullScreen />}
        <PlayerProfileHeader
          changeBackground={this.state.changeBackground}
          changeAvatar={this.state.changeAvatar}
          avatar={this.state.avatar_preview}
          background_image={this.state.background_image_preview}
          first_name={this.state.first_name}
          display_name={this.state.display_name}
          last_name={this.state.last_name}
          headline={this.state.headline}
          location={this.state.location}
          inputAvatarId="upload-avatar-picture"
          inputBackgroundId="upload-bg-picture"
        />
        <CardContent>
          <form onSubmit={event => event.preventDefault()}>
            <DesktopInput
              required
              label="first-name"
              name="first_name"
              id="first_name_id"
              value={this.state.first_name}
              onChange={e => this.handleChange(e.target.value, "first_name")}
              disabled={this.props.isLoading}
            />
            <DesktopInput
              label="display-name"
              name="display_name"
              id="display_name_id"
              value={this.state.display_name}
              onChange={e => this.handleChange(e.target.value, "display_name")}
              disabled={this.props.isLoading}
            />
            <DesktopInput
              required
              label="last-name"
              name="last_name"
              id="last_name_id"
              value={this.state.last_name}
              onChange={e => this.handleChange(e.target.value, "last_name")}
              disabled={this.props.isLoading}
            />
            <DesktopInput
              label="headline"
              name="headline"
              id="headline_id"
              value={this.state.headline}
              onChange={e => this.handleChange(e.target.value, "headline")}
              disabled={this.props.isLoading}
            />
            {/*<DesktopInput
              required
              label="location"
              name="location"
              id="location_id"
              value={this.state.location}
              onChange={e => this.handleChange(e.target.value, "location")}
              disabled={this.props.isLoading}
            />*/}

            <input
              disabled={this.props.isLoading}
              style={{ display: "none" }}
              id="upload-bg-picture"
              type="file"
              accept="image/*"
              onChange={e => {
                const fileList = e.target.files;
                let file = null;
                for (let i = 0; i < fileList.length; i++) {
                  if (fileList[i].type.match(/^image\//)) {
                    file = fileList[i];
                    break;
                  }
                }
                if (file !== null) {
                  this.handleChange(file, "background_image");
                  this.setState({
                    changeBackground: false,
                    background_image_preview: URL.createObjectURL(file)
                  });
                }
              }}
            />

            <input
              disabled={this.props.isLoading}
              style={{ display: "none" }}
              id="upload-avatar-picture"
              type="file"
              accept="image/*"
              onChange={e => {
                const fileList = e.target.files;
                let file = null;
                for (let i = 0; i < fileList.length; i++) {
                  if (fileList[i].type.match(/^image\//)) {
                    file = fileList[i];
                    break;
                  }
                }
                if (file !== null) {
                  this.handleChange(file, "avatar");
                  this.setState({
                    changeAvatar: false,
                    avatar_preview: URL.createObjectURL(file)
                  });
                }
              }}
            />
            <ButtonContent>
              <Button
                type="submit"
                color="primary"
                variant="raised"
                id="SignUpLastStep_send_id"
                onClick={this.savePlayerProfile}
              >
                <FormattedMessage id="submit" />
              </Button>
            </ButtonContent>
          </form>
        </CardContent>
      </Content>
    );
  }

  handleChange = (value, prop) => {
    this.setState({ [prop]: value });
  };

  savePlayerProfile = () => {
    var formData = new FormData();
    const body = this.state;
    const { token, userProfile, save } = this.props;

    // Only append to FormData the inputs that changed
    if (body.first_name !== userProfile.first_name)
      formData.append("first_name", body.first_name);
    if (body.last_name !== userProfile.last_name)
      formData.append("last_name", body.last_name);
    if (body.display_name !== userProfile.display_name)
      formData.append("display_name", body.display_name);
    if (body.headline !== userProfile.headline)
      formData.append("headline", body.headline);
    if (body.avatar !== userProfile.avatar && body.avatar !== null)
      formData.append("avatar", body.avatar);
    if (
      body.background_image !== userProfile.background_image &&
      body.background_image !== null
    )
      formData.append("background_image", body.background_image);
    if (body.location !== userProfile.location)
      formData.append("location", body.location);

    save(token, formData, "/home");
  };
}
// Redux Store / State
const mapStateToProps = state => ({
  userProfile: state.user.profile,
  isLoading: state.app.is_loading,
  token: state.user.token
});
// Redux dispatch
const mapDispatchToProps = dispatch => ({
  save: (token, formData, goTo) =>
    dispatch(savePlayerProfile(token, formData, goTo))
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUpLastStep);
