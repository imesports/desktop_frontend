/**
  All color tags are from the Material Color Palette

  https://material.io/guidelines/style/color.html#color-color-palette

  Create Themes for our aplication in this document.
  Example: PUBG theme, CSGO theme, Main theme, Dark theme...

  When creating a new Theme, use same tags as the ones in landingPageTheme.

  keywords: NewTheme, NewColor, MaterialColor, Theme, Colors
*/

const palette = {
  primary: {
    default: "#673ab7"
  },
  secondary: {
    default: "#607d8b"
  },
  background: {
    appbar: "#FFFFFF",
    footer: "#607D8B",
    body: "#ECEFF1",
    innerCard: "#F5F5F5",
    purple: "#673ab7",
    stepper: "#FFFFFF",
    menuHeader: "#F5F5F5",
    hover: "rgba(0, 0, 0, 0.12);"
  },
  text: {
    primary: "#212121",
    secondary: "#757575",
    footer: "#FAFAFA",
    icon: "#FFFFFF",
    white: "#FFFFFF",
    black: "#000000",
    darkGrey: "#212121",
    lightPurple: "#9575cd"
  },
  misc: {
    signUpButton: "#FFFF00"
  },
  icon: {
    default: "#9E9E9E",
    lightDefault: "#eeeeee",
    primary: "#673ab7",
    verified: "#4caf50",
    bodyContrast: "#212121"
  },
  social: {
    facebook: "#3B5998",
    googleRed: "#D62D20"
  },
  border: {
    light: "#eeeeee",
    default: "#e0e0e0"
  },
  game: {
    PUBG: "#ff8200"
  }
};

const paletteDark = {
  primary: {
    default: "#FFFFFF"
  },
  secondary: {
    default: "#90a4ae"
  },
  background: {
    appbar: "#212121",
    footer: "#607D8B",
    body: "#212121",
    innerCard: "#212121",
    purple: "#673ab7",
    stepper: "#424242",
    menuHeader: "#616161",
    hover: "rgba(0, 0, 0, 0.12);"
  },
  text: {
    primary: "#FFFFFF",
    secondary: "#f5f5f5",
    footer: "#FAFAFA",
    icon: "#FFFFFF",
    white: "#FFFFFF",
    black: "#000000",
    darkGrey: "#212121",
    lightPurple: "#b39ddb"
  },
  misc: {
    signUpButton: "#FFFF00"
  },
  icon: {
    default: "#757575",
    lightDefault: "#616161",
    primary: "#FFFFFF",
    verified: "#4caf50",
    bodyContrast: "#ECEFF1"
  },
  social: {
    facebook: "#3B5998",
    googleRed: "#D62D20"
  },
  border: {
    light: "#303030",
    default: "#616161"
  },
  game: {
    PUBG: "#ff8200"
  }
};

const sizes = {
  borderRadius: {
    xs: 2
  },
  iconSize: {
    default: 48,
    xs: 12,
    small: 20,
    sm: 24,
    medium: 30,
    large: 40,
    xl: 60,
    xxl: 90,
    xxxl: 125
  },
  headerSize: {
    commonToolbar: 56,
    publicHeader: 64,
    loggedInHeader: 104
  },
  cardMediaSize: {
    medium: 100,
    large: 150,
    xl: 200,
    xxl: 300,
    xxxl: 400
  },
  logoSize: {
    public: 300,
    loggedIn: 150
  },
  menuSize: {
    default: 280 /* Remember: also change in Drawer/StyledDrawer */,
    appsMenu: 200,
    pictureMenu: 300,
    notificationsMenu: 450,
    notificationsMenuHeight: 350
  },
  stepperSize: {
    default: 55
  },
  fabSize: {
    default: 56,
    mini: 40
  },
  maxWidthSize: {
    landingTitle: 700,
    loginForm: 500
  },
  contentSize: {
    default: 980,
    columnOneOfTwo: 680,
    columnTwoOfTwo: 280,
    defaultMargin: 8
  },
  minHeightSize: {
    landingFeatureList: 400
  },
  heightDevicesSize: {
    xga: 768,
    HD: 720,
    fullHD: 1080
  },
  widthDevicesSize: {
    xga: 1024,
    HD: 1280,
    fullHD: 1920
  }
};

const spacing = {
  spacing: {
    smallDefault: 8,
    default: 16,
    largeDefault: 24,
    xxxs: 1,
    xxs: 3,
    xs: 5,
    small: 10,
    medium: 15,
    large: 20,
    xl: 25,
    xxl: 30,
    xxxl: 35,
    xxxxl: 40,
    xxxxxl: 50
  }
};

const breakingpoints = {
  breakingpoints: {
    menuBreak: 1200
  }
};

export const mainTheme = ({ type }) => {
  //logic
  const colorPallete = type === "dark" ? paletteDark : palette;

  const theme = {
    // Pallete (All Collors)
    ...colorPallete,
    // Spacing
    ...spacing,
    // Icon sizes
    ...sizes,
    // Breaking Points
    ...breakingpoints
  };

  return theme;
};
