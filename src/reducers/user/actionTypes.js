// Action Types:
export const UPDATE_LOGGED_IN_PROFILE = "user/UPDATE_LOGGED_IN_PROFILE";
export const UPDATE_PLAYER_GAMES = "user/UPDATE_PLAYER_GAMES";
export const UPDATE_PLAYER_TEAMS = "user/UPDATE_PLAYER_TEAMS";
export const UPDATE_TOKEN = "user/UPDATE_TOKEN";
export const LOGOUT = "user/LOGOUT";
