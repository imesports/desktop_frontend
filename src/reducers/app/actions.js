// Import constants
import * as types from "./actionTypes";

export const updateLanguage = language => ({
  type: types.CHANGE_LANGUAGE,
  language
});

export const updateLocale = locale => ({
  type: types.CHANGE_LOCALE,
  locale
});

export const swapTheme = () => ({
  type: types.SWAP_THEME
});

export const loading = loading => ({
  type: types.LOADING,
  loading
});

export const drawerShow = drawerState => ({
  type: types.DRAWER_SHOW,
  drawerState
});

export const gameListLoading = gameListLoading => ({
  type: types.GAME_LIST_LOADING,
  gameListLoading
});

export const teamListLoading = teamListLoading => ({
  type: types.TEAM_LIST_LOADING,
  teamListLoading
});
