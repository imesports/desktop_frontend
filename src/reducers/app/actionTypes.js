// Action Types:
export const GAME_LIST_LOADING = "app/GAME_LIST_LOADING";
export const TEAM_LIST_LOADING = "app/TEAM_LIST_LOADING";
export const CHANGE_LANGUAGE = "app/CHANGE_LANGUAGE";
export const CHANGE_LOCALE = "app/CHANGE_LOCALE";
export const DRAWER_SHOW = "app/DRAWER_SHOW";
export const SWAP_THEME = "app/SWAP_THEME";
export const LOADING = "app/LOADING";
