/**

  After creating a new reducer import it here and export it further

  keywords: NewReducer, Redux, Actions, ActionType

*/

import app from "./app/reducer";
import user from "./user/reducer";

export default { app, user };
