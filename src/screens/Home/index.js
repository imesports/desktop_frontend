// IMPORT libs
import React from "react";
import { connect } from "react-redux";
// IMPORT components
import LoadingFullScreen from "components/UI/Loading/FullScreen";
// Screen: Stateless
const ScreenHome = props => (
  <div>
    {props.is_loading && <LoadingFullScreen />}
    <div>Conteúdo aqui</div>
  </div>
);
// Redux Store / State
const mapStateToProps = state => ({
  is_loading: state.app.is_loading
});

export default connect(mapStateToProps)(ScreenHome);
