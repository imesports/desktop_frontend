import PUBG from "imgs/game-icons/PUBG.svg";
import FORTNITE from "imgs/game-icons/FORTNITE.svg";
import LOL from "imgs/game-icons/LOL.svg";
import DOTA2 from "imgs/game-icons/DOTA2.svg";
import CSGO from "imgs/game-icons/CSGO.svg";

import GuilhermePhoto from "imgs/team/guilherme.jpg";
import GiovanniPhoto from "imgs/team/giovanni.jpg";
import JorgePhoto from "imgs/team/jorge.jpg";
import RaulPhoto from "imgs/team/raul.jpg";

import FacebookBoxIcon from "mdi-react/FacebookBoxIcon";
import InstagramIcon from "mdi-react/InstagramIcon";
import TwitterBoxIcon from "mdi-react/TwitterBoxIcon";
import LinkedinBoxIcon from "mdi-react/LinkedinBoxIcon";

import AllYouNeedImage from "imgs/landing-page/all-you-need-image.jpg";
import AllYouNeedBG from "imgs/landing-page/all-you-need-bg.jpg";
import CrushThemImage from "imgs/landing-page/crush-them-image.jpg";
import CrushThemBG from "imgs/landing-page/crush-them-bg.jpg";
import LoneWolfImage from "imgs/landing-page/lone-wolf-image.jpg";
import LoneWolfBG from "imgs/landing-page/lone-wolf-bg.jpg";
import ShowWhoYouAreImage from "imgs/landing-page/show-who-you-are-image.jpg";
import ShowWhoYouAreBG from "imgs/landing-page/show-who-you-are-bg.jpg";
import LearnOrTeachImage from "imgs/landing-page/learn-or-teach-image.jpg";
import LearnOrTeachBG from "imgs/landing-page/learn-or-teach-bg.jpg";

export default {
  featureList: [
    {
      background: AllYouNeedBG,
      image: AllYouNeedImage,
      title: "desk.landing-page.all-you-need",
      text: "desk.landing-page.all-you-need-text"
    },
    {
      background: ShowWhoYouAreBG,
      image: ShowWhoYouAreImage,
      title: "desk.landing-page.show-who-you-are",
      text: "desk.landing-page.show-who-you-are-text"
    },
    {
      background: LoneWolfBG,
      image: LoneWolfImage,
      title: "desk.landing-page.lone-wolf",
      text: "desk.landing-page.lone-wolf-text"
    },
    {
      background: CrushThemBG,
      image: CrushThemImage,
      title: "desk.landing-page.crush-them-all",
      text: "desk.landing-page.crush-them-all-text"
    },
    {
      background: LearnOrTeachBG,
      image: LearnOrTeachImage,
      title: "desk.landing-page.learn-or-teach",
      text: "desk.landing-page.learn-or-teach-text"
    }
  ],
  supportedGames: [
    {
      icon: PUBG
    },
    {
      icon: FORTNITE
    },
    {
      icon: LOL
    }
  ],
  commingSoonGames: [
    {
      icon: DOTA2
    },
    {
      icon: CSGO
    }
  ],
  teamList: [
    {
      listItemImage: GuilhermePhoto,
      listItemTitle: "guilherme",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/guigmen/"
    },
    {
      listItemImage: GiovanniPhoto,
      listItemTitle: "giovanni",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/giovannicimolin/"
    },
    {
      listItemImage: JorgePhoto,
      listItemTitle: "jorge",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/jorgelourenci/"
    },
    {
      listItemImage: RaulPhoto,
      listItemTitle: "raul",
      listItemSubTitle: "founder",
      linkedInUrl: "https://www.linkedin.com/in/raulguedert/"
    }
  ],
  socialNetworks: [
    {
      link: "https://www.facebook.com/IMeSportsGG/",
      icon: FacebookBoxIcon
    },
    {
      link: "https://www.instagram.com/IMeSportsGG/",
      icon: InstagramIcon
    },
    {
      link: "https://twitter.com/IMeSportsGG/",
      icon: TwitterBoxIcon
    },
    {
      link: "https://www.linkedin.com/company/imesports/",
      icon: LinkedinBoxIcon
    }
  ]
};
