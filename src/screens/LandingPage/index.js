// IMPORT libs
import React, { Component } from "react";
// IMPORT util
import scrollTo from "util/helpers/scrollTo";
// IMPORT image
import SignUpSectionBackground from "imgs/landing-page/sign-up-bg.jpg";
// IMPORT components
import LandingPageSignUpAndLoginSection from "components/LandingPage/Section/SignUpAndLogin";
import LandingPageSignUpActionSection from "components/LandingPage/Section/SignUpAction";
import LandingPageFeaturesList from "components/LandingPage/Features/List";
import LandingPageGamesSection from "components/LandingPage/Section/Games";
import LandingPageTeamSection from "components/LandingPage/Section/Team";
import LandingPageFooter from "components/LandingPage/Footer";
// IMPORT initialization
import initialization from "./initialization";
// Screen: Stateful
class ScreenLandingPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      action: ""
    };
  }

  render() {
    return (
      <div>
        <LandingPageSignUpAndLoginSection
          clickFunction={this.handleLogin}
          background={SignUpSectionBackground}
          action={this.state.action}
        />
        <LandingPageGamesSection
          supportedGames={initialization.supportedGames}
          commingSoonGames={initialization.commingSoonGames}
        />
        <LandingPageFeaturesList list={initialization.featureList} />
        <LandingPageSignUpActionSection
          clickFunction={this.handleLogin("action", "signup")}
        />
        <LandingPageTeamSection cardsList={initialization.teamList} />
        <LandingPageFooter
          subtitle="social-networks"
          socialNetworks={initialization.socialNetworks}
        />
      </div>
    );
  }

  handleLogin = (stateName, state) => () => {
    this.setState({ [stateName]: state });
    scrollTo(document.documentElement, 0, 500);
  };
}

export default ScreenLandingPage;
