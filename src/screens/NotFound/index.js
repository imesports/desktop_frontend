import React from "react";
import styled from "styled-components";

const DivMargin = styled.div`
   {
    padding: 30px;
  }
`;

const ScreenNotFound = () => (
  <DivMargin>
    <span>Page Not Found</span>
  </DivMargin>
);

export default ScreenNotFound;
