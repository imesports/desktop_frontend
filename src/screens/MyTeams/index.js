import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import Card from "material-ui/Card";
import Divider from "material-ui/Divider";

import TeamsListAvatarNameMembersGame from "components/Team/List/AvatarNameMembersGame";
import CardContent from "components/UI/Card/Content";
import MyTeamsActions from "components/Team/Actions";

import CardWrapper from "components/UI/Wrapper/Card";
import {
  MainContainerWrapper,
  Column1of2,
  Column2of2
} from "components/UI/Wrapper/Container";

const ListItemMargin = styled.div`
   {
    margin-bottom: ${props => props.theme.spacing.default}px;
    :last-child {
      margin-bottom: 0px;
    }
  }
`;

const ScreenMyTeams = props => {
  const { userId, teamList } = props;

  return (
    <MainContainerWrapper>
      <Column1of2>
        <CardWrapper>
          {teamList.length > 0 && (
            <Card>
              <CardContent>
                {teamList.map((content, index) => (
                  <ListItemMargin key={`MyTeamsScreen ${content.id}`}>
                    <TeamsListAvatarNameMembersGame
                      avatar={content.avatar}
                      teamName={content.name}
                      teamMembers={1}
                      teamId={content.id}
                      gameName={content.game.name}
                      userId={userId}
                      ownerId={content.ownerId}
                      memberSince="MONTH/YEAR"
                    />
                    {teamList.length !== index + 1 && <Divider />}
                  </ListItemMargin>
                ))}
              </CardContent>
            </Card>
          )}
        </CardWrapper>
      </Column1of2>
      <Column2of2>
        <MyTeamsActions />
      </Column2of2>
    </MainContainerWrapper>
  );
};

const mapStateToProps = state => ({
  teamList: state.user.teams,
  userId: state.user.profile.user
});

export default connect(mapStateToProps)(ScreenMyTeams);
