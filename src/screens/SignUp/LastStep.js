// IMPORT libs
import React from "react";
import styled from "styled-components";
// IMPORT material
import Card from "material-ui/Card";
// IMPORT components
import { MaxWidthSignUpWrapper } from "components/UI/Wrapper/Container";
import SignUpUsernameHeader from "components/SignUp/LastStep/Header";
import SignUpLastStep from "components/SignUp/LastStep";
import CardContent from "components/UI/Card/Content";
import CardWrapper from "components/UI/Wrapper/Card";
// Custom Styles
const StyledCard = styled(Card)`
   {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;
// Screen: Stateless
const ScreenSignUpLastStep = props => (
  <MaxWidthSignUpWrapper>
    <CardWrapper>
      <StyledCard>
        <CardContent>
          <SignUpUsernameHeader
            title="sign-up2.title"
            subtitle="sign-up2.subtitle"
          />
        </CardContent>
        <SignUpLastStep />
      </StyledCard>
    </CardWrapper>
  </MaxWidthSignUpWrapper>
);

export default ScreenSignUpLastStep;
