// IMPORT libs
import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { compose } from "redux";
// IMPORT util
import { profileFetch } from "util/user";
// IMPORT components
import PlayerProfileHeaderCard from "components/Player/Profile/Card/Header";
import {
  MainContainerWrapper,
  Column1of2,
  Column2of2
} from "components/UI/Wrapper/Container";
// Screen: Stateful
class ScreenPlayerProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playerInfo: null,
      loading: false,
      error: false
    };
  }

  componentDidMount() {
    const { token } = this.props;
    const { player_id } = this.props.match.params;
    this.getProfile(token, player_id);
  }

  componentWillReceiveProps(nextProps) {
    const { player_id } = nextProps.match.params;
    if (this.props.match.params.player_id !== player_id) {
      const { token } = this.props;
      this.getProfile(token, player_id);
    }
  }

  render() {
    const { playerInfo, error, loading } = this.state;
    if (loading) {
      return <div>Loading</div>;
    }
    return (
      <div>
        {playerInfo && (
          <MainContainerWrapper>
            <Column1of2>
              <PlayerProfileHeaderCard userData={playerInfo} />
            </Column1of2>
            <Column2of2>b</Column2of2>
          </MainContainerWrapper>
        )}
        {error && <MainContainerWrapper>Algo deu errado.</MainContainerWrapper>}
      </div>
    );
  }

  getProfile = (token, player_id) => {
    this.setState({ error: false });
    this.setState({ loading: true });
    profileFetch(token, player_id).then(res => {
      this.setState({ loading: false });
      if (res.ok) {
        res.json().then(player => {
          // Treatment of Error (user didn't go through the SignUp Last Step)
          if (player.first_name === "") {
            this.setState({ error: true });
          } else {
            this.setState({ playerInfo: player });
          }
        });
      } else {
        // Treatment of Error (user don't exist)
        this.setState({ error: true });
      }
    });
  };
}

const mapStateToProps = state => ({
  token: state.user.token
});

export default compose(connect(mapStateToProps), withRouter)(
  ScreenPlayerProfile
);
