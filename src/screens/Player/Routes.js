/* Imports from libraries */
import React from "react";
import { Switch } from "react-router";
/* Import Components */
import HeaderWithDrawer from "components/AppHeader/HeaderWithDrawer";
import PrivateRoute from "components/Route/PrivateRoute";
import asyncComp from "components/AsyncComponent";
/* Import Screens */
const ScreenPlayerProfile = asyncComp(() => import("screens/Player/Profile"));
// All /player routes
const PlayerRoutes = props => (
  <div>
    <HeaderWithDrawer appComponent={props.appComponent} />
    <div>
      <Switch>
        {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
        <PrivateRoute
          exact
          path={`${props.match.url}/:player_id`}
          component={ScreenPlayerProfile}
        />
      </Switch>
    </div>
  </div>
);

export default PlayerRoutes;
