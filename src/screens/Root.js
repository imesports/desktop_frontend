/**
  Add all screens in the file, add it's route by choosing a proper URL for the container

  keywords: Router, Route, NewRoute, URL, Path
*/
// IMPORT libs
import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Route, Switch } from "react-router";
// IMPORT material
import { MuiThemeProvider } from "material-ui/styles";
import Reboot from "material-ui/Reboot";
// IMPORT Globals
import CONSTANTS from "appConstants";
import { MaterialTheme } from "theme/materialtheme";
import { mainTheme } from "theme";
// IMPORT components
import PrivateRoute from "components/Route/PrivateRoute";
import PublicRoute from "components/Route/PublicRoute";
import asyncComp from "components/AsyncComponent";
import AppHeader from "components/AppHeader";
// IMPORT screen
import ScreenLandingPage from "./LandingPage";
import RoutesMyTeams from "screens/MyTeams/Routes";
import RoutesMyGames from "screens/MyGames/Routes";
import RoutesPlayer from "screens/Player/Routes";
const ScreenSignUpLastStep = asyncComp(() => import("./SignUp/LastStep"));
const ScreenNotFound = asyncComp(() => import("./NotFound"));
const ScreenHome = asyncComp(() => import("./Home"));

class App extends Component {
  render() {
    const theme_color = localStorage.getItem(CONSTANTS.THEME_COLOR);
    return (
      <MuiThemeProvider theme={MaterialTheme({ type: theme_color })}>
        <ThemeProvider theme={mainTheme({ type: theme_color })}>
          <div>
            <Reboot />
            {/* Equivalent as normalize.css */}

            <AppHeader appComponent={this} />
            <div>
              <Switch>
                {/* PUBLIC ROUTES */}
                <PublicRoute exact path="/" component={ScreenLandingPage} />
                {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
                <PrivateRoute exact path="/home" component={ScreenHome} />
                {/* You can use here as Route because inside RoutesMyGames, all routes are PrivateRoute*/}
                <Route
                  path="/my-games"
                  render={props => (
                    <RoutesMyGames {...props} appComponent={this} />
                  )}
                />
                <Route
                  path="/my-teams"
                  render={props => (
                    <RoutesMyTeams {...props} appComponent={this} />
                  )}
                />
                <Route
                  path="/player"
                  render={props => (
                    <RoutesPlayer {...props} appComponent={this} />
                  )}
                />
                <PrivateRoute
                  exact
                  path="/signup/last-step"
                  component={ScreenSignUpLastStep}
                  noLeftMargin
                />
                {/* UNMATCHED ROUTES - UPDATE */}
                <Route component={ScreenNotFound} />
              </Switch>
            </div>
          </div>
        </ThemeProvider>
      </MuiThemeProvider>
    );
  }
}

export default App;
