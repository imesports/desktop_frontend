/* Imports from libraries */
import React from "react";
import { Switch } from "react-router";
/* Import Components */
import HeaderWithDrawer from "components/AppHeader/HeaderWithDrawer";
import PrivateRoute from "components/Route/PrivateRoute";
import asyncComp from "components/AsyncComponent";
/* Import Screens */
const ScreenMyGames = asyncComp(() => import("screens/MyGames"));
// All /my-games routes
const MyGamesRoutes = props => (
  <div>
    <HeaderWithDrawer appComponent={props.appComponent} />
    <div>
      <Switch>
        {/* PROTECTED ROUTES: USER HAS TO BE LOGGED IN */}
        <PrivateRoute exact path={props.match.url} component={ScreenMyGames} />
      </Switch>
    </div>
  </div>
);

export default MyGamesRoutes;
