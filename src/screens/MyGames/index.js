// IMPORT libs
import React from "react";
import { FormattedMessage } from "react-intl";
import { connect } from "react-redux";
// IMPORT material
import { CircularProgress } from "material-ui/Progress";
import Typography from "material-ui/Typography";
// IMPORT components
import GameCard from "components/Game/Card/Game";
import Panel from "components/Game/Add/Panel";
// IMPORT styles
import {
  MainContainerWrapper,
  Column1of2,
  Column2of2
} from "components/UI/Wrapper/Container";

const MyGamesScreen = props => {
  const { playerGames, token, userId, gameListLoading } = props;

  return (
    <MainContainerWrapper>
      <Column1of2>
        {gameListLoading ? (
          <div style={{ textAlign: "center" }}>
            <CircularProgress color="primary" />
          </div>
        ) : playerGames.length > 0 ? (
          playerGames.map(content => (
            <GameCard
              gameId={content.id}
              key={`MyGamesItemKey ${content.name}`}
              name={content.name}
              backgroundImage={content.background_image}
              lastUpdatedTime="xh atrás"
              statsName=""
              stats="X%"
              statsDiff="Y%"
              goTo={`/my-games/${content.name}`}
            />
          ))
        ) : (
          <div>
            <Typography variant="body1" align="center">
              <FormattedMessage id="games.you-dont-have-a-game" />
            </Typography>
            <Typography variant="body1" align="center">
              <FormattedMessage id="games.click-on-right-menu-to-add" />
            </Typography>
          </div>
        )}
      </Column1of2>
      <Column2of2>
        {token &&
          userId && (
            <Panel token={token} userId={userId} playerGames={playerGames} />
          )}
      </Column2of2>
    </MainContainerWrapper>
  );
};

const mapStateToProps = state => ({
  gameListLoading: state.app.gameListLoading,
  userId: state.user.profile.user,
  playerGames: state.user.games,
  token: state.user.token
});

export default connect(mapStateToProps)(MyGamesScreen);
