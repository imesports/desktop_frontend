// Declare all APP level constants
export default {
  DEFAULT_LOCALE: "en-US",
  default_backgorund_img: "https://i.imgur.com/9od0mSo.jpg",
  default_avatar_img:
    "https://cdn.pixabay.com/photo/2017/02/23/13/05/profile-2092113_1280.png",
  BACKEND_URL: "https://api-next.imesports.gg",
  AUTH_TOKEN: "auth-token",
  THEME_COLOR: "theme-color"
};
