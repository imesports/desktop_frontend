import CONSTANTS from "appConstants";

export const createTeamFetch = (token, body) =>
  fetch(`${CONSTANTS.BACKEND_URL}/teams/`, {
    method: "POST",
    headers: {
      Authorization: `JWT ${token}`
    },
    body
  });

export const playerTeamsFetch = (token, userId) =>
  fetch(`${CONSTANTS.BACKEND_URL}/player-profile/${userId}/teams/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });

export const playersInTeamFetch = (token, teamId) => {
  fetch(`${CONSTANTS.BACKEND_URL}/team/${teamId}/teams/`, {
    headers: {
      Authorization: `JWT ${token}`
    }
  });
};
