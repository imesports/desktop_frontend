import React from "react";
import ReactSVG from "react-svg";

import CSGOsvg from "imgs/game-icons/CSGO.svg";
import FORTNITEsvg from "imgs/game-icons/FORTNITE.svg";
import LOLsvg from "imgs/game-icons/LOL.svg";
import PUBGsvg from "imgs/game-icons/PUBG.svg";

import GuyFawkesMaskIcon from "mdi-react/GuyFawkesMaskIcon";

const GamesIcon = props => {
  switch (props) {
    case "csgo":
      return <ReactSVG path={CSGOsvg} />;

    case "fortnite":
      return <ReactSVG path={FORTNITEsvg} />;

    case "pubg":
      return <ReactSVG path={PUBGsvg} />;

    case "lol":
      return <ReactSVG path={LOLsvg} />;

    default:
      return <GuyFawkesMaskIcon />;
  }
};

export default GamesIcon;
