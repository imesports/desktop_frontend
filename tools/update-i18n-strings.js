#!/usr/bin/env node

const USAGE = 'usage: update-i18n-strings <output directory>'

if (process.argv.length < 3) {
  console.error(USAGE)
  process.exit(1)
}

var path = require('path')
var Promise = require('bluebird')
var GoogleSpreadsheet = require('google-spreadsheet')
var promisifyAll = Promise.promisifyAll
var fs = promisifyAll(require('fs'))

const OUTPUT_DIRECTORY = process.argv[2]
const CREDENTIALS_FILE = '.i18n-spreadsheet-credential.json'
const CREDENTIALS = require(path.resolve(__dirname, CREDENTIALS_FILE))
const SPREADSHEET = '1kHhmGFVmv-CxqWo8QlRVcRYhLlYOTCwl2ke0iS158Oc'
const WORKSHEET = 0
const LANGUAGES = {
  pt: {},
  en: {},
  es: {}
}

function errorExit(message) {
  console.error(message)
  process.exit(1)
}

function exitSucess() {
  console.log('i18n strings updated sucessfully')
  process.exit(0)
}

function reduceRows(rows) {
  return rows.reduce((memo, row) => {
    var obj = Object.assign({}, memo)
    for (var lang in LANGUAGES) {
      obj[lang][row.key] = row[lang]
    }
    return obj
  }, LANGUAGES)
}

function writeLanguageFiles(languages) {
  return Promise.all(
    Object.keys(languages).map(language => {
      var contents = JSON.stringify(languages[language], undefined, 2)
      return fs.writeFileAsync(
        path.join(OUTPUT_DIRECTORY, `${language}.json`),
        contents
      )
    })
  )
}

fs.statAsync(OUTPUT_DIRECTORY)
  .catch(() => errorExit(`${OUTPUT_DIRECTORY} does not exist.`))
  .then(stat => stat.isDirectory() ? Promise.resolve() : Promise.reject())
  .catch(() => errorExit(`${OUTPUT_DIRECTORY} is not a directory.`))

var spreadsheet = promisifyAll(new GoogleSpreadsheet(SPREADSHEET))

spreadsheet.useServiceAccountAuthAsync(CREDENTIALS)
  .then(() => spreadsheet.getInfoAsync())
  .then(info => info.worksheets[WORKSHEET])
  .then(promisifyAll)
  .then(sheet => sheet.getRowsAsync({ offset: 1 }))
  .then(reduceRows)
  .then(writeLanguageFiles)
  .then(exitSucess)
  .catch(error => errorExit(error))
